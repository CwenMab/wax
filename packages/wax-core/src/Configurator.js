import { Configurator as SubstanceConfigurator } from 'substance'

class Configurator extends SubstanceConfigurator {
  constructor(...props) {
    super(...props)
    this.config.providers = {}
  }

  addProvider(name, provider) {
    this.config.providers[name] = provider
  }

  getProviders(name = '') {
    return name === '' ? this.config.providers : this.config.providers['name']
  }
}

export default Configurator
