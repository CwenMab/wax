import { Component, Toolbar } from 'substance'

class SideToolbar extends Component {
  render($$) {
    const { configurator } = this.props
    const menu = configurator.menus.sideToolBar
    const wrapper = $$('div').addClass('sidenav')
    const sideNav = $$('div')
      .addClass('se-toolbar-wrapper')
      .append(
        $$(Toolbar, {
          toolPanel: configurator.getToolPanel(menu),
        }),
      )
    return wrapper.append(sideNav)
  }
}

export default SideToolbar
