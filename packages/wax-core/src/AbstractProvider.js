import {
  clone,
  eachRight,
  find,
  forEach,
  includes,
  isEmpty,
  map,
  pickBy,
  sortBy,
  last,
} from 'lodash'

import { EventEmitter } from 'substance'

class AbstractProvider extends EventEmitter {
  constructor(document, config) {
    super(document, config)
    this.document = document
    this.config = config
    this.editorSession = config.editorSession
    this.entries = this.computeEntries()

    if (this.entries.length > 0) {
      this.activeEntry = this.entries[0].id
    } else {
      this.activeEntry = null
    }
  }

  dispose() {
    const doc = this.getDocument()
    doc._disconnect(this)
  }

  // Inspects a document change and recomputes the
  // entries if necessary
  handleDocumentChange(change) {
    console.warn('this method is Abstract')
  }

  computeEntries() {
    const doc = this.getDocument()
    const nodes = doc.getNodes()
    const ref = this
    const allNodes = {}
    // get all nodes from the document
    forEach(ref.constructor.NodeType, type => {
      const typeNodes = pickBy(nodes, (value, key) => value.type === type)
      Object.assign(allNodes, typeNodes)
    })
    return this.sortNodes(allNodes)
  }

  // TODO FIX THIS MESS
  sortNodes(nodes) {
    let currentNodes = clone(nodes)
    const containers = this.getAllAvailableSurfaces()

    // sort notes by
    // the index of the containing block
    // their position within that block
    currentNodes = map(currentNodes, node => {
      const blockId = node.path[0]

      let container = containers.find(cont => {
        if (includes(cont.nodes, blockId)) {
          return cont
        }
      })
      // This is actually not correct.
      // TODO if container not found selection
      // might be for example a list-item
      // Now works as lists only are allowed in main surface.
      // Fix as to find the right container for the selection.
      if (!container) container = containers[0]

      const blockPosition = container.getPosition(blockId)
      const nodePosition = node.start.offset

      return {
        id: node.id,
        blockPosition,
        nodePosition,
        node,
        containerId: container.id,
        containerType: container.type,
        order: container.order ? container.order : 0,
      }
    })

    return sortBy(currentNodes, ['order', 'blockPosition', 'nodePosition'])
  }

  sortNodesBySurface() {
    // TODO implement
  }

  getProviderNode(id) {
    const node = find(this.entries, { id })
    return node
  }

  getAllAvailableSurfaces() {
    const nodes = this.document.getNodes()
    const containers = []

    forEach(nodes, node => {
      if (node._isContainer) {
        containers.push(node)
      }
    })
    return containers
  }

  getNextNode() {
    const { editorSession } = this
    const selection = editorSession.getSelection()
    const container = this.getContainerForSelection(selection)
    const selectionBlockPositon = container.getPosition(selection.path[0])
    const matches = this.getEntries()
    let found = ''

    forEach(matches, match => {
      if (
        match.containerId === container.id &&
        match.blockPosition === selectionBlockPositon &&
        match.node.start.offset > selection.start.offset
      ) {
        found = match
        return false
      } else if (
        match.containerId === container.id &&
        match.blockPosition > selectionBlockPositon
      ) {
        found = match
        return false
      } else if (match.containerId !== container.id) {
        const same = matches.filter(m => {
          return m.containerId === container.id
        })
        if (isEmpty(same)) {
          found = matches[0]
        } else {
          const index = matches.findIndex(
            x =>
              x.node.start === last(same).node.start &&
              x.node.end === last(same).node.end,
          )
          found = matches[index + 1]
        }
      } else {
        found = matches[0]
      }
    })
    if (found) return found
    return false
  }

  getPreviousNode() {
    const { editorSession } = this
    const selection = editorSession.getSelection()
    const container = this.getContainerForSelection(selection)
    const selectionBlockPositon = container.getPosition(selection.path[0])
    const matches = this.getEntries()
    let found = ''

    eachRight(matches, match => {
      if (
        match.containerId === container.id &&
        match.blockPosition === selectionBlockPositon &&
        match.node.start.offset < selection.start.offset
      ) {
        found = match
        return false
      } else if (
        match.containerId === container.id &&
        match.blockPosition < selectionBlockPositon
      ) {
        found = match
        return false
      } else if (match.containerId !== container.id) {
        const same = matches.filter(m => {
          return m.containerId === container.id
        })
        if (isEmpty(same)) {
          found = last(matches)
        } else {
          const index = matches.findIndex(
            x =>
              x.node.start === last(same).node.start &&
              x.node.end === last(same).node.end,
          )
          found = matches[index - 1]
        }
      } else {
        found = last(matches)
      }
    })
    if (found) return found
    return false
  }

  getContainerForSelection(selection) {
    const containersEls = this.getAllAvailableSurfaces(this.document)
    const selectionNodeId = selection.getNodeId()

    let container = containersEls.find(cont => {
      if (includes(cont.nodes, selectionNodeId)) {
        return cont
      }
    })
    if (!container) container = containersEls[0]
    return container
  }

  hasEntries() {
    return !isEmpty(this.getEntries())
  }

  getEntries() {
    return this.computeEntries()
  }

  getEntriesLengthByType(type) {
    const doc = this.document
    const nodes = doc.getNodes()
    const allNodes = pickBy(nodes, (value, key) => value.type === type)

    return Object.keys(allNodes).length
  }

  getDocument() {
    return this.document
  }

  getActiveSurface() {
    return this.editorSession.surfaceManager.getFocusedSurface()
  }
}

export default AbstractProvider
