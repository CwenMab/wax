import {
  BasePackage,
  ProseArticle,
  Document as SubstanceDocument,
} from 'substance'

//setup
import MainNode from './setup/MainNode'
import MainConverter from './setup/MainConverter'
import defaultToolPanels from './setup/defaultToolPanels'
import ucpToolPanels from './setup/ucpToolPanels'
// My Elements
import AuthorPackage from './elements/author/AuthorPackage'
import BibliographyEntryPackage from './elements/bibliographyEntry/BibliographyEntryPackage'
import PersistencePackage from './elements/persistence/PersistencePackage'
import ChapterSubtitlePackage from './elements/chapterSubtitle/ChapterSubtitlePackage'
import ChapterTitlePackage from './elements/chapterTitle/ChapterTitlePackage'
import CommentPackage from './elements/comment/CommentPackage'
import DiacriticsPackage from './elements/diacritics/DiacriticsPackage'
import FindAndReplacePackage from './elements/find_and_replace/FindAndReplacePackage'
import EpigraphPoetryPackage from './elements/epigraphPoetry/EpigraphPoetryPackage'
import EpigraphProsePackage from './elements/epigraphProse/EpigraphProsePackage'
import ExtractPoetryPackage from './elements/extractPoetry/ExtractPoetryPackage'
import ExtractProsePackage from './elements/extractProse/ExtractProsePackage'
import ImagePackage from './elements/images/ImagePackage'
import LinkPackage from './elements/link/LinkPackage'
import NotePackage from './elements/note/NotePackage'
import NotePanePackage from './panes/Notes/NotePanePackage'
import ParagraphContdPackage from './elements/paragraphContd/ParagraphContdPackage'
import ParagraphPackage from './elements/paragraph/ParagraphPackage'
import SmallCapsPackage from './elements/small_caps/SmallCapsPackage'
import SubscriptPackage from './elements/subscript/SubscriptPackage'
import SuperscriptPackage from './elements/superscript/SuperscriptPackage'
import StrongPackage from './elements/strong/StrongPackage'
import EmphasisPackage from './elements/emphasis/EmphasisPackage'
import SourceNotePackage from './elements/source_note/SourceNotePackage'
import SwitchSurfacePackage from './elements/switchsurface/SwitchSurfacePackage'
import TrackChangePackage from './elements/track_change/TrackChangePackage'
import FullScreenPackage from './elements/fullscreen/FullScreenPackage'
import HighLighterPackage from './elements/highlighter/HighLighterPackage'
import ShortCutsModalPackage from './elements/shortcuts_modal/ShortCutsModalPackage'
import ScriptPackage from './elements/script/ScriptPackage'
import CodePackage from './elements/code/CodePackage'
import OrnamentPackage from './elements/ornament/OrnamentPackage'
import SpellCheckPackage from './elements/spellCheck/SpellCheckPackage'
import HeadingPackage from './elements/headings/HeadingPackage'
import SpellCheckTogglePackage from './elements/spellcheck_toggle/SpellCheckTogglePackage'
import ChangeCasePackage from './elements/change_case/ChangeCasePackage'
import QuoteMarksPackage from './elements/quote_mark/QuoteMarksPackage'
import ListPackage from './elements/list/ListPackage'
import TablePackage from './elements/table/TablePackage'
// import InlineNotePackage from './elements/inline_note/InlineNotePackage'

const config = {
  name: 'simple-editor',
  configure: (config, options) => {
    config.defineSchema({
      name: 'prose-article',
      version: '1.0.0',
      DocumentClass: SubstanceDocument,
      defaultTextType: 'paragraph',
    })
    // Editor Nodes
    config.addNode(MainNode)
    config.addConverter('html', MainConverter)

    config.import(BasePackage, {
      noBaseStyles: options.noBaseStyles,
    })

    config.import(defaultToolPanels)
    config.import(ucpToolPanels)

    // Block Level Packages
    config.import(ChapterTitlePackage)
    config.import(ChapterSubtitlePackage)
    config.import(AuthorPackage)
    config.import(ParagraphPackage)
    config.import(ParagraphContdPackage)
    config.import(ExtractProsePackage)
    config.import(ExtractPoetryPackage)
    config.import(EpigraphProsePackage)
    config.import(EpigraphPoetryPackage)
    config.import(BibliographyEntryPackage)
    config.import(SourceNotePackage)
    config.import(HeadingPackage)

    config.import(TablePackage)
    config.import(StrongPackage)
    config.import(EmphasisPackage)
    config.import(SmallCapsPackage)
    config.import(SubscriptPackage)
    config.import(SuperscriptPackage)
    config.import(ScriptPackage)
    config.import(LinkPackage)
    config.import(PersistencePackage)
    config.import(CodePackage)
    config.import(OrnamentPackage)
    config.import(CommentPackage)
    config.import(ImagePackage)
    config.import(ListPackage)
    config.import(NotePackage)
    config.import(NotePanePackage)
    config.import(SpellCheckPackage)
    config.import(TrackChangePackage)
    config.import(DiacriticsPackage)
    config.import(FindAndReplacePackage)
    config.import(SwitchSurfacePackage)
    config.import(HighLighterPackage)
    config.import(FullScreenPackage)
    config.import(ShortCutsModalPackage)
    config.import(SpellCheckTogglePackage)
    config.import(QuoteMarksPackage)
    config.import(ChangeCasePackage)

    // config.import(InlineNotePackage)
  },
}

export default config
