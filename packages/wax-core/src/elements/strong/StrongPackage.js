import WaxAnnotationCommand from '../../commands/WaxAnnotationCommand'
import Strong from './Strong'
import StrongHTMLConverter from './StrongHTMLConverter'
import StrongComponent from './StrongComponent'

export default {
  name: 'strong',
  configure: function(config) {
    config.addNode(Strong)
    config.addConverter('html', StrongHTMLConverter)
    config.addComponent('strong', StrongComponent)
    config.addCommand('strong', WaxAnnotationCommand, {
      nodeType: 'strong',
      commandGroup: 'insert-strong',
    })
    config.addIcon('strong', { fontawesome: 'fa-bold' })
    config.addLabel('strong', {
      en: 'Strong',
      de: 'Fett',
    })
    config.addKeyboardShortcut('CommandOrControl+B', { command: 'strong' })
  },
  Strong,
  StrongComponent,
  StrongHTMLConverter,
}
