import { Command } from 'substance'

class FullScreenCommand extends Command {
  getCommandState(params) {
    let newState = {
      active: false,
      disabled: false,
    }
    if (params.surface) {
      const editorProps = params.surface.context.editor.props
      if (editorProps.editing === 'selection') newState.disabled = true
    }

    return newState
  }

  execute(params) {
    const surface = params.surface
    const editor = surface.context.editor
    editor.extendState({ fullScreen: !editor.state.fullScreen })
    if (!params.selection.isNull()) {
      params.editorSession.setSelection(params.selection)
    }
  }
}

export default FullScreenCommand
