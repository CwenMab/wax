import ParagraphContd from './ParagraphContd'
import ParagraphContdComponent from './ParagraphContdComponent'
import ParagraphContdHTMLConverter from './ParagraphContdHTMLConverter'
import WaxSwitchTextTypeCommand from '../../commands/WaxSwitchTextTypeCommand'

export default {
  name: 'paragraph-contd',
  configure: function(config) {
    config.addNode(ParagraphContd)
    config.addComponent(ParagraphContd.type, ParagraphContdComponent)
    config.addConverter('html', ParagraphContdHTMLConverter)
    config.addCommand('paragraph-contd', WaxSwitchTextTypeCommand, {
      spec: { type: 'paragraph-contd' },
      commandGroup: 'text-types',
    })
    // config.addIcon('paragraph-contd', { fontawesome: 'fa-paragraph' })
    config.addLabel('paragraph-contd', {
      en: `General Text Cont'd`,
    })
    config.addKeyboardShortcut('CommandOrControl+Alt+1', {
      command: 'paragraph-contd',
    })
  },
  ParagraphContd: ParagraphContd,
  ParagraphContdComponent: ParagraphContdComponent,
  ParagraphContdHTMLConverter: ParagraphContdHTMLConverter,
}
