import EpigraphProse from './EpigraphProse'
import EpigraphProseComponent from './EpigraphProseComponent'
import EpigraphProseHTMLConverter from './EpigraphProseHTMLConverter'
import WaxSwitchTextTypeCommand from '../../commands/WaxSwitchTextTypeCommand'

export default {
  name: 'epigraph-prose',
  configure: config => {
    config.addNode(EpigraphProse)
    config.addCommand('epigraph-prose', WaxSwitchTextTypeCommand, {
      spec: { type: 'epigraph-prose' },
      commandGroup: 'text-display',
    })
    config.addComponent(EpigraphProse.type, EpigraphProseComponent)
    config.addConverter('html', EpigraphProseHTMLConverter)

    config.addLabel('epigraph-prose', {
      en: 'Epigraph: Prose',
    })
  },
}
