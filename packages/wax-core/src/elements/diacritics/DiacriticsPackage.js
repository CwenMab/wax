import { platform } from 'substance'
import DiacriticsCommand from './DiacriticsCommand'
import DiacriticsControlTool from './DiacriticsControlTool'
import DiacriticsShortCutCommand from './DiacriticsShortCutCommand'

export default {
  name: 'diacritics',
  configure: (config, { toolGroup }) => {
    config.addTool('diacritics-tool', DiacriticsControlTool)
    config.addCommand('diacritics-tool', DiacriticsCommand, {
      commandGroup: 'diacritics-tool',
    })
    config.addIcon('diacritics-tool', { fontawesome: ' fa-ellipsis-h' })
    config.addLabel('diacritics-tool', {
      en: 'Special characters tool',
    })

    config.addCommand(
      'diacritics-shortcut-acute-accent',
      DiacriticsShortCutCommand,
      {
        character: '\u00B4',
      },
    )

    config.addCommand(
      'diacritics-shortcut-grave-accent',
      DiacriticsShortCutCommand,
      {
        character: '\u0060',
      },
    )

    //  config.addCommand('diacritics-shortcut-umlaut', DiacriticsShortCutCommand, {
    //    character: '\u00B4'
    //  })

    // config.addCommand('diacritics-shortcut-circumflex', DiacriticsShortCutCommand, {
    //   character: '\u00B4'
    // })

    config.addCommand(
      'diacritics-shortcut-c-with-cedilla',
      DiacriticsShortCutCommand,
      {
        character: '\u00E7',
      },
    )

    config.addCommand(
      'diacritics-shortcut-latin-small-ligature-oe',
      DiacriticsShortCutCommand,
      {
        character: '\u0153',
      },
    )

    config.addCommand('diacritics-shortcut-bullet', DiacriticsShortCutCommand, {
      character: '\u2022',
    })

    let altKey = 'alt'
    if (platform.isMac) {
      altKey = 'ctrl'
    }

    config.addKeyboardShortcut(`${altKey}+e`, {
      description: 'enter acute accent special character',
      command: 'diacritics-shortcut-acute-accent',
    })

    // config.addKeyboardShortcut(`${altKey}+`, {
    //   command: 'diacritics-shortcut-grave-accent'
    // })

    config.addKeyboardShortcut(`${altKey}+c`, {
      description: 'enter c with cedilla special character',
      command: 'diacritics-shortcut-c-with-cedilla',
    })

    config.addKeyboardShortcut(`${altKey}+q`, {
      description: 'enter latin small ligature oe special character',
      command: 'diacritics-shortcut-latin-small-ligature-oe',
    })

    config.addKeyboardShortcut(`${altKey}+8`, {
      description: 'enter bullet special character',
      command: 'diacritics-shortcut-bullet',
    })
  },
}
