import { TextBlockComponent } from 'substance'

class EpigraphPoetryComponent extends TextBlockComponent {
  render($$) {
    const el = super.render($$)
    return el.addClass('sc-epigraph-poetry')
  }
}

export default EpigraphPoetryComponent
