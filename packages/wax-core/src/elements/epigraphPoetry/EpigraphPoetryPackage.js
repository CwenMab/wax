import EpigraphPoetry from './EpigraphPoetry'
import EpigraphPoetryComponent from './EpigraphPoetryComponent'
import EpigraphPoetryHTMLConverter from './EpigraphPoetryHTMLConverter'
import WaxSwitchTextTypeCommand from '../../commands/WaxSwitchTextTypeCommand'

export default {
  name: 'epigraph-poetry',
  configure: config => {
    config.addNode(EpigraphPoetry)
    config.addCommand('epigraph-poetry', WaxSwitchTextTypeCommand, {
      spec: { type: 'epigraph-poetry' },
      commandGroup: 'text-display',
    })
    config.addComponent(EpigraphPoetry.type, EpigraphPoetryComponent)
    config.addConverter('html', EpigraphPoetryHTMLConverter)

    config.addLabel('epigraph-poetry', {
      en: 'Epigraph: Poetry',
    })
  },
}
