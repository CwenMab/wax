import Author from './Author'
import AuthorComponent from './AuthorComponent'
import AuthorHTMLConverter from './AuthorHTMLConverter'
import WaxSwitchTextTypeCommand from '../../commands/WaxSwitchTextTypeCommand'

export default {
  name: 'author',
  configure: config => {
    config.addNode(Author)
    config.addComponent(Author.type, AuthorComponent)
    config.addConverter('html', AuthorHTMLConverter)
    config.addCommand('author', WaxSwitchTextTypeCommand, {
      spec: { type: 'author' },
      commandGroup: 'text-display',
    })
    config.addIcon('author', { fontawesome: 'fa-author' })
    config.addLabel('author', {
      en: 'Author',
    })
    config.addKeyboardShortcut('CommandOrControl+Alt+0', {
      command: 'author',
    })
  },
  Author: Author,
  AuthorComponent: AuthorComponent,
  AuthorHTMLConverter: AuthorHTMLConverter,
}
