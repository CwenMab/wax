import {
  each,
  includes,
  pickBy,
  forEach,
  isEmpty,
  last,
  eachRight,
} from 'lodash'
import { documentHelpers, Marker } from 'substance'

import TrackChange from '../track_change/TrackChange'

const getContainers = doc => {
  const nodes = doc.getNodes()
  const containersEls = []

  each(nodes, node => {
    if (node._isContainer) {
      containersEls.push(node)
    }
  })
  return containersEls
}

const getContainerForSelection = (selection, doc) => {
  const containersEls = getContainers(doc)
  const selectionNodeId = selection.getNodeId()
  let container = ''
  each(containersEls, cont => {
    if (includes(cont.nodes, selectionNodeId)) {
      container = cont
    }
  })

  return container
}

const clearMatches = doc => {
  const nodes = doc.getNodes()

  const typeNodes = pickBy(
    nodes,
    (value, key) => value.type === 'find-and-replace',
  )

  each(typeNodes, node => {
    doc.delete(node.id)
  })
}

const findInTextProperty = ({
  path,
  containerId,
  findString,
  editorSession,
}) => {
  const doc = editorSession.document
  const text = doc.get(path)

  let match
  const allMatches = []
  const matcher = new RegExp(findString, 'ig')

  do {
    match = matcher.exec(text)
    if (match) {
      allMatches.push(match)
    }
  } while (match)

  const matches = []
  each(allMatches, m => {
    const marker = new Marker(doc, {
      type: 'property',
      id: path[0],
      start: {
        path,
        offset: m.index,
      },
      end: {
        offset: m.index + m[0].length,
      },
    })
    const container = doc.get(containerId)

    marker.blockPosition = container.getPosition(path[0])
    marker.containerId = containerId
    matches.push(marker)
  })
  return matches
}

const excludeTrackChanges = (matches, doc) => {
  eachRight(matches, (match, index) => {
    const trackChange = documentHelpers.getPropertyAnnotationsForSelection(
      doc,
      match.getSelection(),
      { type: 'track-change' },
    )

    if (trackChange.length >= 1) {
      matches.splice(index, 1)
    }
  })
  return matches
}

const highLightMatches = (matches, doc, surfaceManager) => {
  clearMatches(doc)

  if (matches.length !== 0) {
    each(matches, match => {
      doc.create({
        type: 'find-and-replace',
        start: match.start,
        end: match.end,
        path: match.path,
        containerId: match.containerId,
        surfaceId: match.containerId,
      })
    })
  }

  const containersEls = getContainers(doc)
  each(containersEls, container => {
    surfaceManager.getSurface(container.id).rerender()
  })
}

const creteTrackDeletionAndInsert = (
  nodes,
  replaceValue,
  user,
  editorSession,
) => {
  editorSession.transaction(tx => {
    eachRight(nodes, node => {
      tx.create({
        status: 'delete',
        type: 'track-change',
        containerId: node.containerId,
        surfaceId: node.containerId,
        path: node.path,
        start: node.start,
        end: node.end,
        user: {
          id: user.id,
          username: user.username,
        },
      })

      TrackChange.autoExpandRight = false

      tx.setSelection({
        type: 'property',
        containerId: node.containerId,
        surfaceId: node.containerId,
        path: node.path,
        startOffset: node.end.offset,
        endOffset: node.end.offset,
      })
      tx.insertText(replaceValue)

      tx.create({
        status: 'add',
        type: 'track-change',
        containerId: node.containerId,
        surfaceId: node.containerId,
        path: node.path,
        startOffset: node.end.offset,
        endOffset: node.end.offset + replaceValue.length,
        user: {
          id: user.id,
          username: user.username,
        },
      })
    })
  })
}

const findNextNode = (matches, container, selection, selectionBlockPositon) => {
  let found = ''

  forEach(matches, match => {
    if (
      match.containerId === container.id &&
      match.blockPosition === selectionBlockPositon &&
      match.start.offset > selection.start.offset
    ) {
      found = match
      return false
    } else if (
      match.containerId === container.id &&
      match.blockPosition > selectionBlockPositon
    ) {
      found = match
      return false
    } else if (match.containerId !== container.id) {
      const same = matches.filter(m => {
        return m.containerId === container.id
      })
      if (isEmpty(same)) {
        found = matches[0]
      } else {
        const index = matches.findIndex(
          x => x.start === last(same).start && x.end === last(same).end,
        )
        found = matches[index + 1]
      }
    } else {
      found = matches[0]
    }
  })
  return found
}

const findPreviousNode = (
  matches,
  container,
  selection,
  selectionBlockPositon,
) => {
  let found = ''

  eachRight(matches, match => {
    if (
      match.containerId === container.id &&
      match.blockPosition === selectionBlockPositon &&
      match.start.offset < selection.start.offset
    ) {
      found = match
      return false
    } else if (
      match.containerId === container.id &&
      match.blockPosition < selectionBlockPositon
    ) {
      found = match
      return false
    } else if (match.containerId !== container.id) {
      const same = matches.filter(m => {
        return m.containerId === container.id
      })
      if (isEmpty(same)) {
        found = last(matches)
      } else {
        const index = matches.findIndex(
          x => x.start === last(same).start && x.end === last(same).end,
        )
        found = matches[index - 1]
      }
    } else {
      found = last(matches)
    }
  })
  return found
}

const createSelection = (editorSession, node) => {
  editorSession.transaction(tx => {
    tx.setSelection({
      type: 'property',
      containerId: node.containerId,
      surfaceId: node.containerId,
      path: node.path,
      startOffset: node.start.offset,
      endOffset: node.end.offset,
    })
  })
}

export {
  clearMatches,
  createSelection,
  creteTrackDeletionAndInsert,
  excludeTrackChanges,
  getContainers,
  findInTextProperty,
  findNextNode,
  findPreviousNode,
  getContainerForSelection,
  highLightMatches,
}
