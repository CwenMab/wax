import { Command } from 'substance'

class FindAndReplaceCommand extends Command {
  getCommandState(params) {
    const sel = params.selection
    const surface = params.surface
    const newState = {
      active: false,
      disabled: true,
    }

    if (surface && surface.props.editing === 'selection') {
      newState.disabled = true
      return newState
    }

    if (
      sel &&
      !sel.isNull() &&
      !sel.isCustomSelection() &&
      surface &&
      surface.isContainerEditor()
    ) {
      newState.disabled = false
    }
    return newState
  }

  execute(params) {
    // TODO -- body should not be here explicitly
    const surface = params.editorSession.surfaceManager.getSurface('main')
    surface.context.editor.emit('toggleModal', 'findAndReplaceModal')
  }
}

FindAndReplaceCommand.type = 'find-and-replace-tool'

export default FindAndReplaceCommand
