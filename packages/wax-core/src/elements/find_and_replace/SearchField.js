/* eslint react/prop-types: 0 */
import { debounce } from 'lodash'
import { Component } from 'substance'

class SearchField extends Component {
  didMount() {
    setTimeout(() => {
      this.refs['search-field'].el.focus()
    }, 0)
  }

  render($$) {
    const searchInput = $$('div')
      .addClass('find-and-replace-search-container')
      .append(
        $$('div')
          .addClass('search-text')
          .append('Search for :'),
      )
      .append(
        $$('input')
          .addClass('search-input')
          .attr('id', 'search-text')
          .attr('name', 'search')
          .attr('value', '')
          .attr('autocomplete', 'off')
          .attr('autofocus', true)
          .on('keyup', debounce(this.props.onKeyUp, 300))
          .on('keydown', this.props.enterSearch)
          .on('focus', debounce(this.props.onFocus, 300))
          .ref('search-field'),
      )
    return searchInput
  }
}

export default SearchField
