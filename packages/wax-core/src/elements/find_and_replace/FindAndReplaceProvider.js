import { each, includes } from 'lodash'
import { Marker } from 'substance'
import AbstractProvider from '../../AbstractProvider'

class FindAndReplaceProvider extends AbstractProvider {
  static get NodeType() {
    return ['find-and-replace']
  }

  static initialize(context) {
    const config = {
      commandManager: context.commandManager,
      containerId: context.props.containerId,
      controller: context,
      editorSession: context.editorSession,
      surfaceManager: context.surfaceManager,
      user: context.props.user,
    }
    return new FindAndReplaceProvider(context.doc, config)
  }
}
export default FindAndReplaceProvider
