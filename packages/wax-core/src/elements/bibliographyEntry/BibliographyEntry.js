import { TextBlock } from 'substance'

class BibliographyEntry extends TextBlock {}

BibliographyEntry.type = 'bibliography-entry'

export default BibliographyEntry
