import BibliographyEntry from './BibliographyEntry'
import BibliographyEntryComponent from './BibliographyEntryComponent'
import BibliographyEntryHTMLConverter from './BibliographyEntryHTMLConverter'
import WaxSwitchTextTypeCommand from '../../commands/WaxSwitchTextTypeCommand'

export default {
  name: 'bibliographyEntry',
  configure: config => {
    config.addNode(BibliographyEntry)
    config.addCommand('bibliographyEntry', WaxSwitchTextTypeCommand, {
      spec: { type: 'bibliography-entry' },
      commandGroup: 'text-types',
    })
    config.addComponent(BibliographyEntry.type, BibliographyEntryComponent)
    config.addConverter('html', BibliographyEntryHTMLConverter)

    config.addLabel('bibliographyEntry', {
      en: 'Bibliography Entry',
    })
  },
}
