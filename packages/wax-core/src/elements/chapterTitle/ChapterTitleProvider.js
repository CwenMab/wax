import { isEmpty, keys } from 'lodash'
import AbstractProvider from '../../AbstractProvider'

class ChapterTitleProvider extends AbstractProvider {
  static get NodeType() {
    return ['chapter-title']
  }

  static initialize(context) {
    const { update } = context.props
    const config = { update }

    return new ChapterTitleProvider(context.doc, config)
  }

  constructor(document, config) {
    super(document, config)
    document.on('document:saved', this.save, this)
  }

  dispose() {
    document.off('document:saved')
  }

  getTitle() {
    const nodeEntries = this.computeEntries()
    const chapterTitleNode = keys(nodeEntries)

    return !isEmpty(chapterTitleNode)
      ? nodeEntries[chapterTitleNode[0]].content
      : null
  }

  save() {
    const title = this.getTitle()
    this.config.update({ title })
  }

  // TODO -- doesn't sortNodes return them anyway? why overwrite?
  // Should be Created multiple Abstract Types
  sortNodes(nodes) {
    return nodes
  }
}

export default ChapterTitleProvider
