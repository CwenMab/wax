import ChapterTitle from './ChapterTitle'
import ChapterTitleComponent from './ChapterTitleComponent'
import ChapterTitleHTMLConverter from './ChapterTitleHTMLConverter'
import ChapterTitleProvider from './ChapterTitleProvider'
import ChapterTitleCommand from './ChapterTitleCommand'

export default {
  name: 'chapter-title',
  configure: config => {
    config.addNode(ChapterTitle)

    config.addComponent(ChapterTitle.type, ChapterTitleComponent)
    config.addConverter('html', ChapterTitleHTMLConverter)
    config.addCommand('chapter-title', ChapterTitleCommand, {
      spec: { type: 'chapter-title' },
      commandGroup: 'text-display',
    })
    config.addLabel('chapter-title', {
      en: 'Title',
    })

    config.addProvider('chapterTitleProvider', ChapterTitleProvider)
  },
}
