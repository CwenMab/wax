import { groupBy, forEach, first, uniq, sortBy } from 'lodash'
import { Command } from 'substance'

class SwitchSurfaceCommand extends Command {
  getCommandState(params) {
    const newState = {
      active: false,
      disabled: false,
    }

    return newState
  }

  execute(params, context) {
    const editorSession = params.editorSession
    const sel = editorSession.getSelection()
    const doc = editorSession.getDocument()
    const nodes = doc.getNodes()
    const container = doc.get(sel.containerId)
    const containersEls = []
    let types = []

    forEach(nodes, node => {
      if (node._isContainer) {
        containersEls.push(node)
        types.push(node.type)
      }
    })

    types = uniq(types, type => {
      return type
    })

    const data = groupBy(containersEls, 'type')
    let position = 0

    forEach(types, (type, index) => {
      if (type === container.type) position = index
    })

    if (position === types.length - 1) position = -1
    const nextType = types[position + 1]
    const sorted = sortBy(data[nextType], 'order')

    editorSession.transaction(tx => {
      tx.setSelection({
        type: 'property',
        containerId: sorted[0].id,
        surfaceId: sorted[0].id,
        startOffset: 0,
        endOffset: 0,
        path: [first(sorted[0].nodes), 'content'],
      })
    })
  }
}

SwitchSurfaceCommand.type = 'switch-surface'

export default SwitchSurfaceCommand
