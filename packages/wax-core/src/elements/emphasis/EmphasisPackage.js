import Emphasis from './Emphasis'
import EmphasisHTMLConverter from './EmphasisHTMLConverter'
import EmphasisComponent from './EmphasisComponent'
import WaxAnnotationCommand from '../../commands/WaxAnnotationCommand'

export default {
  name: 'emphasis',
  configure: function(config) {
    config.addNode(Emphasis)
    config.addConverter('html', EmphasisHTMLConverter)
    config.addComponent('emphasis', EmphasisComponent)
    config.addCommand('emphasis', WaxAnnotationCommand, {
      nodeType: Emphasis.type,
      commandGroup: 'annotations',
    })
    config.addIcon('emphasis', { fontawesome: 'fa-italic' })
    config.addLabel('emphasis', {
      en: 'Emphasis',
      de: 'Betonung',
    })
    config.addKeyboardShortcut('CommandOrControl+I', { command: 'emphasis' })
  },
  Emphasis,
  EmphasisComponent,
  EmphasisHTMLConverter,
}
