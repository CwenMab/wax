import SourceNote from './SourceNote'
import SourceNoteComponent from './SourceNoteComponent'
import SourceNoteHTMLConverter from './SourceNoteHTMLConverter'
import WaxSwitchTextTypeCommand from '../../commands/WaxSwitchTextTypeCommand'

export default {
  name: 'source-note',
  configure: config => {
    config.addNode(SourceNote)
    config.addCommand('source-note', WaxSwitchTextTypeCommand, {
      spec: { type: 'source-note' },
      commandGroup: 'text-types',
    })
    config.addCommand('source-note-display', WaxSwitchTextTypeCommand, {
      spec: { type: 'source-note' },
      commandGroup: 'text-display',
    })
    config.addComponent(SourceNote.type, SourceNoteComponent)
    config.addConverter('html', SourceNoteHTMLConverter)

    // config.addIcon('source-note', { fontawesome: 'fa-book' })
    config.addLabel('source-note', {
      en: 'Source Note',
    })
    config.addLabel('source-note-display', {
      en: 'Source Note',
    })
  },
}
