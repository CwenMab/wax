import { AnnotationCommand } from 'substance'

class SourceNoteCommand extends AnnotationCommand {}

SourceNoteCommand.type = 'source-note'

export default SourceNoteCommand
