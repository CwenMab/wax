import { minBy, maxBy } from 'lodash'
import { AnnotationCommand } from 'substance'

class CommentCommand extends AnnotationCommand {
  // Override function to allow overlapping comments
  canCreate(annos, sel, params) {
    if (!sel.isCollapsed()) {
      if (annos.length === 0) return true
      if (this.isSelectionLargerThanComments(annos, sel)) return true
    }

    return false
  }

  execute(params, context) {
    const { mode } = params.commandState

    // If on a comment, simply move focus to comment input
    if (mode === 'fuse' || mode === 'delete') {
      const annos = this._getAnnotationsForSelection(params)
      const anno = annos[0]
      this.moveFocusToComment(params, anno)
      return
    }

    // Execute command and store new annotation
    const newComment = super.execute(params, context)
    params.surface.context.editor.emit('newComment')
    const annotation = newComment.anno
    this.moveFocusToComment(params, annotation)
  }

  // Get comments provider
  getProvider(params) {
    const { surface } = params
    const { editor } = surface.context
    const provider = editor.getChildContext().commentsProvider

    return provider
  }

  // Set new comment as active and move focus there
  moveFocusToComment(params, annotation) {
    const provider = this.getProvider(params)

    provider.setActiveEntry(annotation.id)
    provider.focusTextArea(annotation.id)
  }

  /*
    DUPLICATE CODE WITH PROVIDER
    TODO -- refactor
  */
  isSelectionLargerThanComments(annos, selection) {
    const commentBorders = this.getCommentBorders(annos)
    if (!commentBorders) return false

    if (
      selection.start.offset < commentBorders.start ||
      selection.end.offset > commentBorders.end
    )
      return true

    return false
  }

  // returns the combined borders of all comment annotations in selection
  getCommentBorders(comments) {
    if (comments.length === 0) return null

    const minComment = minBy(comments, 'start.offset')
    const maxComment = maxBy(comments, 'end.offset')

    const min = minComment.start.offset
    const max = maxComment.end.offset

    return {
      start: min,
      end: max,
    }
  }
  /*
    END DUPLICATE CODE
  */
}

CommentCommand.type = 'comment'

export default CommentCommand
