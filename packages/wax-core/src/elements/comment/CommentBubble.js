import { forEach } from 'lodash'
import { DefaultDOMElement, FontAwesomeIcon as Icon, Tool } from 'substance'

class CommentBubble extends Tool {
  render($$) {
    if (!this.canCreateComment()) return $$('div')

    const el = super.render($$)

    const iconPlus = $$(Icon, { icon: 'fa-plus' }).addClass(
      'sc-comment-icon-plus',
    )

    el.addClass('sc-overlay-bubble')
      .addClass('sc-overlay-bubble-hidden')
      .append(iconPlus)

    return el
  }

  // reset bubble position on window resize
  // only works on the horizontal level, as the vertical position gets
  // calculated relative to the overlay container, which gets positioned
  // wrong on resize (substance bug -- TODO)
  didMount() {
    this.context.editorSession.onUpdate('', this.position, this)
    this.position()
    DefaultDOMElement.getBrowserWindow().on('resize', this.didUpdate, this)
  }

  didUpdate() {
    this.position()
  }

  dispose() {
    DefaultDOMElement.getBrowserWindow().off(this)
  }

  position() {
    if (this.el.getChildCount() === 0) return
    this.setBubblePosition()
  }

  setBubblePosition() {
    const { containerId } = this.context.editor.props

    const documentElement = document.querySelector(
      `div[data-id="${containerId}"]`,
    )
    const overlayContainerAll = document.querySelectorAll(`.sc-overlay`)

    let overlayContainerLeft = 0
    setTimeout(() => {
      // read comment below
      forEach(overlayContainerAll, overlayContainer => {
        if (overlayContainer.offsetLeft !== 0) {
          overlayContainerLeft = overlayContainer.offsetLeft
        }
      })
      let documentElementWidth = 0

      if (documentElement) documentElementWidth = documentElement.offsetWidth
      const left = documentElementWidth - overlayContainerLeft

      // unhide it first, as the bubble has no height otherwise
      this.el.removeClass('sc-overlay-bubble-hidden')

      const wsel = window.getSelection()
      const wrange = wsel.getRangeAt(0)
      const hints = wrange.getBoundingClientRect()
      const selectionHeight = hints.height
      const bubbleHeight = this.el.getHeight()
      const moveUp = selectionHeight / 2 + bubbleHeight / 2
      const top = '-' + moveUp + 'px'

      this.el.css('left', left)
      this.el.css('top', top)
    })

    /*
      There is a race condition with overlayContainer's position.
      If it gets rendered fast enough, this is fine.
      Otherwise, the overlayContainerLeft variable won't have been updated by
      substance for us to get the correct value.
      There is no event letting us know that this has been updated,
      and it's probably not worth creating a listener.
    */
  }

  getCommentState() {
    const { commandManager } = this.context
    const { commandStates } = commandManager
    return commandStates.comment
  }

  getMode() {
    const commentState = this.getCommentState()
    return commentState.mode
  }

  getProvider() {
    return this.context.commentsProvider
  }

  getSurface() {
    const provider = this.getProvider()
    return provider.getActiveSurface()
  }

  isSelectionLargerThanComments() {
    const provider = this.getProvider()
    return provider.isSelectionLargerThanComments()
  }

  canCreateComment() {
    const mode = this.getMode()

    if (mode === 'create') return true
    if (!this.isSelectionLargerThanComments()) return false

    return true
  }
}

export default CommentBubble
