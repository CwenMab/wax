import { PropertyAnnotation } from 'substance'

class ResolvedComment extends PropertyAnnotation {}

ResolvedComment.schema = {
  type: 'resolved-comment',
  data: {
    type: 'array',
    default: [],
  },
}

export default ResolvedComment
