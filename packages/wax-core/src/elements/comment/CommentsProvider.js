import _ from 'lodash'
import { documentHelpers } from 'substance'
import AbstractProvider from '../../AbstractProvider'

class CommentsProvider extends AbstractProvider {
  static get NodeType() {
    return ['comment']
  }

  static initialize(context) {
    const { commandManager, editorSession, props, surfaceManager } = context
    const { comments, containerId, update } = props

    const controller = context
    const name = 'comments'

    const config = {
      commandManager,
      comments,
      containerId,
      controller,
      editorSession,
      name,
      surfaceManager,
      update,
    }

    return new CommentsProvider(context.doc, config)
  }

  constructor(doc, props) {
    super(doc, props)

    this.activeEntry = null
    const editorSession = props.editorSession
    doc.on('document:changed', this.handleDocumentChange, this)
    editorSession.onUpdate('', this.updateActiveEntry, this)
    editorSession.onRender('document', this.CheckIfCommentToUpdate, this)

    const editor = props.controller
    editor.on('ui:updated', this.onUiUpdate, this)
  }

  handleDocumentChange(change) {
    const commentCreated = _
      .values(change.created)
      .filter(value => value.type === 'comment')
    const commentDeleted = _
      .values(change.deleted)
      .filter(value => value.type === 'comment')
    if (!_.isEmpty(commentCreated) || !_.isEmpty(commentDeleted)) {
      this.entries = this.computeEntries()
    }
  }

  update() {
    this.emit('comments:updated')
  }

  CheckIfCommentToUpdate() {
    const selectionContainsComments = this.selectionContainsComments()
    if (!selectionContainsComments) return

    this.update()
  }

  // entries

  // TODO -- this will probably not be needed if we stop moving the
  // line height for track changes
  // offset by the time it takes the animation to change the line height
  onUiUpdate() {
    setTimeout(() => {
      this.update()
    }, 100)
  }

  reComputeEntries() {
    this.entries = this.computeEntries()
    this.update()
  }

  //
  // active entry

  getActiveEntry() {
    return this.activeEntry
  }

  setActiveEntry(id) {
    this.removeActivePropFromNode(this.activeEntry)
    this.addActivePropToNode(id)

    this.activeEntry = id
    this.update()
  }

  removeActiveEntry() {
    this.removeActivePropFromNode(this.activeEntry)

    this.activeEntry = null
    this.update()
  }

  // runs when document session is updated
  updateActiveEntry() {
    const activeEntry = this.getActiveEntry()
    const selectionContainsComments = this.selectionContainsComments()

    // neither current or previous selection is a comment, ignore
    if (!selectionContainsComments && !activeEntry) return

    // previous selection was a comment, but new one isn't
    // or the selection contains a comment, but is larger than it,
    // allowing you to make an overlapping comment
    // in both cases, remove the active comment
    const isSelectionLargerThanComments = this.isSelectionLargerThanComments()
    if (
      (!selectionContainsComments && activeEntry) ||
      isSelectionLargerThanComments
    ) {
      this.removeEmptyComments()
      this.removeActiveEntry()
      return
    }

    const activeComment = this.getActiveComment()
    if (!activeComment) return this.removeActiveEntry()

    // current selection and previous selection both belong to the same comment
    if (activeEntry === activeComment.id) return

    // new active comment
    this.setActiveEntry(activeComment.id)
  }

  //
  // resolved comments

  resolveComment(id) {
    const es = this.config.editorSession
    const commentNode = this.getProviderNode(id)
    const path = commentNode.node.path
    const startOffset = commentNode.node.start.offset
    const endOffset = commentNode.node.end.offset
    const containerId = commentNode.containerId
    const sel = es.createSelection({
      type: 'property',
      path,
      startOffset,
      endOffset,
    })

    const resolvedNodeData = {
      containerId,
      selection: sel,
      type: 'resolved-comment',
      path: sel.path,
      start: sel.start,
      end: sel.end,
    }

    let newSel = ''
    // create resolved comment annotation on the same selection the
    // comment was on and remove existing comment
    es.transaction((tx, args) => {
      tx.create(resolvedNodeData)
      newSel = tx.createSelection({
        type: 'property',
        containerId,
        surfaceId: containerId,
        path: sel.path,
        startOffset: sel.end.offset,
        endOffset: sel.end.offset,
      })
    })

    this.deleteCommentNode(id)
    this.reComputeEntries()
    es.setSelection(newSel)
  }

  //
  // focus text area for a comment reply

  focusTextArea(id) {
    setTimeout(() => {
      const textarea = document.getElementById(id)
      if (textarea) {
        textarea.focus()
      }
    }, 10)
  }

  focusEditor() {
    const editorSession = this.getEditorSession()
    const selection = editorSession.getSelection()

    editorSession.setSelection(selection)
  }

  //
  // remove comments from pane if the user wrote nothing in them

  removeEmptyComments() {
    const activeComment = this.getNode(this.getActiveEntry())
    if (!activeComment || !_.isEmpty(activeComment.discussion)) return
    this.deleteEmptyCommentNode(activeComment.id)
  }

  //
  //
  // helpers
  //

  deleteCommentNode(id) {
    const commentNode = this.getProviderNode(id)
    const containerId = commentNode.containerId
    const es = this.config.editorSession
    const surfaces = es.surfaceManager.surfaces
    const surface = _.find(surfaces, { containerId })
    es.transaction((tx, args) => {
      tx.delete(id)
      return args
    })
    surface.rerender()
  }

  deleteEmptyCommentNode(id) {
    const commentNode = this.getProviderNode(id)
    const containerId = commentNode.containerId
    const es = this.config.editorSession
    const surfaces = es.surfaceManager.surfaces
    const surface = _.find(surfaces, { containerId })
    setTimeout(() => {
      es.transaction((tx, args) => {
        tx.delete(id)
        return args
      })
    })
  }

  getActiveComment() {
    const comments = this.getSelectionComments()
    const mode = this.getMode()
    const selection = this.getSelection()

    // we have a non-collapsed selection that contains multiple comments
    // if (mode === 'fuse') {
    // only keep the comments that contain the whole selection
    let annos = _.filter(comments, annotation => {
      return annotation.getSelection().contains(selection)
    })

    if (annos.length === 0) return null
    if (annos.length === 1) return annos[0]

    // find comments that contain all other comments and remove them
    // the focus of the active comment should be inwards
    const remove = []
    _.forEach(annos, anno => {
      let removeIt = true

      // TODO -- there's gotta be a better way to do this than a double loop
      _.forEach(annos, innerAnno => {
        if (anno === innerAnno) return

        const outerSelection = anno.getSelection()
        const innerSelection = innerAnno.getSelection()
        if (!outerSelection.contains(innerSelection)) removeIt = false
      })

      if (removeIt) remove.push(anno)
    })

    annos = _.difference(annos, remove)

    // once you have all comments that do not contain each other wholly
    // just choose the one on the left
    return _.minBy(annos, 'start.offset')
    // }

    return comments[0]
  }

  // returns an array of all comments in selection
  getSelectionComments() {
    const comments = documentHelpers.getPropertyAnnotationsForSelection(
      this.config.editorSession.document,
      this.config.editorSession.getSelection(),
      { type: 'comment' },
    )
    return comments
  }

  getComments() {
    const doc = this.getDocument()
    const nodes = doc.getNodes()

    // get all notes from the document
    const comments = _.pickBy(nodes, (value, key) => {
      return value.type === 'comment'
    })
    return comments
  }

  getCommentState() {
    const { commandManager } = this.config
    const { commandStates } = commandManager
    return commandStates.comment
  }

  getMode() {
    const commentState = this.getCommentState()
    return commentState.mode
  }

  getNode(id) {
    const doc = this.getDocument()
    return doc.get(id)
  }

  getEditorSession() {
    return this.config.editorSession
  }

  // returns the combined borders of all comment annotations in selection
  getCommentBorders() {
    const comments = this.getSelectionComments()
    if (comments.length === 0) return

    const minComment = _.minBy(comments, 'start.offset')
    const maxComment = _.maxBy(comments, 'end.offset')

    const min = minComment.start.offset
    const max = maxComment.end.offset

    return {
      start: min,
      end: max,
    }
  }

  getSelection() {
    const editorSession = this.getEditorSession()
    return editorSession.getSelection()
  }

  getSurface() {
    const { containerId, surfaceManager } = this.config
    return surfaceManager.getSurface(containerId)
  }

  isSelectionLargerThanComments() {
    const commentBorders = this.getCommentBorders()
    if (!commentBorders) return false

    const selection = this.getSelection()

    if (
      selection.start.offset < commentBorders.start ||
      selection.end.offset > commentBorders.end
    )
      return true

    return false
  }

  selectionContainsComments() {
    const commentAnnotations = this.getSelectionComments()
    return commentAnnotations.length > 0
  }

  addActivePropToNode(id) {
    this.updateActivePropForNode(id, true)
  }

  removeActivePropFromNode(id) {
    this.updateActivePropForNode(id, false)
  }

  updateActivePropForNode(id, bool) {
    if (!id) return
    const node = this.getNode(id)
    if (!node) return
    node.active = bool
  }
}

export default CommentsProvider
