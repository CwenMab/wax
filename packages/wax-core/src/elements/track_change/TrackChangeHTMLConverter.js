export default {
  type: 'track-change',
  tagName: 'track-change',

  import: function(element, node, converter) {
    node.status = element.attr('status')
    node.user = {
      id: element.attr('user-id'),
      username: element.attr('username'),
    }
  },

  export: function(node, element, converter) {
    const { status, user, oldType, addedType } = node

    element.setAttribute('status', status)
    element.setAttribute('user-id', user.id)
    element.setAttribute('username', user.username)
  },
}
