import { forEach, sortBy } from 'lodash'
import { Command, documentHelpers } from 'substance'

class TrackChangeAcceptCommand extends Command {
  getCommandState(params) {
    let newState = {
      active: false,
      disabled: true,
    }

    if (!params.surface || !this.isViewOn(params)) {
      return newState
    }

    if (params.surface) {
      const editorProps = params.surface.context.editor.props
      const trackChangesPermissions = editorProps.mode.trackChanges
      if (editorProps.editing === 'selection') return newState
      if (!trackChangesPermissions.others.reject) return newState
    }

    const trackChanges = this.getChanges(params)
    if (trackChanges.length === 0) return newState

    newState = {
      active: true,
      disabled: false,
    }

    return newState
  }

  extracttrackChanges(selection, doc) {
    const types = this.getAnnotationTypes()
    const trackChanges = []
    forEach(types, type => {
      const changes = documentHelpers.getPropertyAnnotationsForSelection(
        doc,
        selection,
        {
          type: type,
        },
      )
      trackChanges.push(changes)
    })
    return [].concat(...trackChanges)
  }

  getChanges(params) {
    const selection = params.editorSession.getSelection()
    const doc = params.editorSession.document
    let trackChanges = []
    if (selection.type === 'container') {
      const propertySelections = selection.splitIntoPropertySelections()

      forEach(propertySelections, property => {
        trackChanges.push(this.extracttrackChanges(property, doc))
      })
      const merged = [].concat(...trackChanges)
      return merged
    }

    trackChanges = this.extracttrackChanges(selection, doc)
    return trackChanges
  }

  execute(params) {
    const provider = this.getProvider(params)
    const action = this.getActionType()
    const annotations = this.getChanges(params)

    const allAnnos = sortBy(annotations, ['start.path[0]', 'start.offset'])
    provider.resolve(allAnnos, action)
  }

  getProvider(params) {
    return params.surface.context.trackChangesProvider
  }

  getAnnotationTypes() {
    return this.config.nodeTypes
  }

  getActionType() {
    return this.config.actionType
  }

  isViewOn(params) {
    const surface = params.surface

    const editor = surface.context.editor
    const { trackChangesView } = editor.state
    return trackChangesView
  }
}

export default TrackChangeAcceptCommand
