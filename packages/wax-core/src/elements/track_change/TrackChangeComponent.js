import { AnnotationComponent } from 'substance'

class TrackChangeComponent extends AnnotationComponent {
  didMount() {
    // empty didMount as to not trigger
    // substance's listener
  }

  render($$) {
    const { id, status, user } = this.props.node
    const viewMode = this.getViewMode()

    const el = $$('span')
      .attr('data-id', id)
      .addClass(this.getClassNames())
      .append(this.props.children)

    if (viewMode === false) {
      if (status === 'delete') el.addClass('sc-track-delete-hide')
      if (status === 'add') el.addClass('sc-track-add-show')
    } else {
      el.attr('title', `${user.username}`)
    }

    const statusClass = `sc-track-change-${status}`
    el.addClass(statusClass)

    return el
  }

  getEditor() {
    return this.context.editor
  }

  getProvider() {
    return this.context.trackChangesProvider
  }

  getViewMode() {
    const editor = this.getEditor()
    const { trackChangesView } = editor.state
    return trackChangesView
  }
}

export default TrackChangeComponent
