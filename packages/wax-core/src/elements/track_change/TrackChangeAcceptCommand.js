import { forEach, sortBy, filter } from 'lodash'
import { Command, documentHelpers } from 'substance'

class TrackChangeAcceptCommand extends Command {
  constructor(...props) {
    super(...props)

    this.excludeInBulk = false
  }

  getCommandState(params) {
    this.excludeInBulk = false

    let newState = {
      active: false,
      disabled: true,
    }

    if (!params.surface || !this.isViewOn(params)) {
      return newState
    }

    if (params.surface) {
      const trackChanges = this.getChanges(params)
      const user = this.getCurrentUser(params)
      const editorProps = params.surface.context.editor.props
      const trackChangesPermissions = editorProps.mode.trackChanges

      if (editorProps.editing === 'selection') return newState

      if (
        trackChanges.length === 1 &&
        trackChanges[0].user.username === user.username &&
        trackChangesPermissions.others.accept &&
        !trackChangesPermissions.own.accept
      ) {
        return newState
      }

      if (
        trackChanges.length > 1 &&
        trackChangesPermissions.others.accept &&
        !trackChangesPermissions.own.accept
      ) {
        newState.active = true
        newState.disabled = false
        this.excludeInBulk = true
        return newState
      }
    }

    const trackChanges = this.getChanges(params)
    if (trackChanges.length === 0) return newState

    newState = {
      active: true,
      disabled: false,
    }

    return newState
  }

  extracttrackChanges(selection, doc) {
    const types = this.getAnnotationTypes()
    const trackChanges = []
    forEach(types, type => {
      const changes = documentHelpers.getPropertyAnnotationsForSelection(
        doc,
        selection,
        {
          type: type,
        },
      )
      trackChanges.push(changes)
    })
    return [].concat(...trackChanges)
  }

  getChanges(params) {
    const selection = params.editorSession.getSelection()
    const doc = params.editorSession.document
    let trackChanges = []
    if (selection.type === 'container') {
      const propertySelections = selection.splitIntoPropertySelections()
      forEach(propertySelections, property => {
        trackChanges.push(this.extracttrackChanges(property, doc))
      })
      const merged = [].concat(...trackChanges)
      return merged
    }

    trackChanges = this.extracttrackChanges(selection, doc)
    return trackChanges
  }

  execute(params) {
    const provider = this.getProvider(params)
    const action = this.getActionType()
    const annotations = this.getChanges(params)
    const user = this.getCurrentUser(params)
    const allAnnos = sortBy(annotations, ['start.path[0]', 'start.offset'])
    if (this.excludeInBulk) {
      let filtered = []

      forEach(allAnnos, anno => {
        if (anno.user.username !== user.username) {
          filtered.push(anno)
        }
      })
      provider.resolve(filtered, action)
      return
    }

    provider.resolve(allAnnos, action)
  }

  getProvider(params) {
    return params.surface.context.trackChangesProvider
  }

  getAnnotationTypes() {
    return this.config.nodeTypes
  }

  getActionType() {
    return this.config.actionType
  }

  isViewOn(params) {
    const surface = params.surface

    const editor = surface.context.editor
    const { trackChangesView } = editor.state
    return trackChangesView
  }

  getCurrentUser(params) {
    const user = params.surface.context.editor.props.user
    return user
  }
}

export default TrackChangeAcceptCommand
