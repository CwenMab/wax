import {
  cloneDeep,
  eachRight,
  filter,
  forEach,
  includes,
  some,
  values,
} from 'lodash'

import { annotationHelpers, documentHelpers } from 'substance'

import TrackChange from './TrackChange'

import {
  getAnnotationByStatus,
  getAllAnnotationsByStatus,
  isAnnotationFromTheSameUser,
  isNotOnTrackAnnotation,
  isOnAnnotation,
  isSelectionOnLeftEdge,
  isSelectionOnRightEdge,
} from './utils/annotationHelpers'

import {
  createAdditionAnnotationOnLastChar,
  deleteAllOwnAdditions,
  insertCharacterWithoutExpandingAnnotation,
  selectCharacterAndMarkDeleted,
} from './utils/handlerHelpers'

import {
  isSelectionCollapsed,
  isSelectionContainedWithin,
  moveCursorTo,
  updateSelection,
} from './utils/selectionHelpers'

import { deleteCharacter, insertText } from './utils/transformations'

import AbstractProvider from '../../AbstractProvider'

const AcceptedNodeTypes = [
  'paragraph',
  'paragraph-contd',
  'heading',
  'epigraph-poetry',
  'bibliographyEntry',
  'chapter-title',
  'epigraph-poetry',
  'epigraph-prose',
  'extract-poetry',
  'extract-prose',
  'source-note',
]

class TrackChangesProvider extends AbstractProvider {
  constructor(document, config) {
    super(document, config)
    // handle button actions
    const editor = this.config.controller

    this.rerenderEditor = false

    editor.handleActions({
      trackChangesViewUpdate: () => {
        editor.extendState({ trackChangesView: !editor.state.trackChangesView })
        editor.emit('ui:updated')
      },
    })
  }

  static get NodeType() {
    return ['track-change', 'track-change-format']
  }

  static initialize(context) {
    const config = {
      commandManager: context.commandManager,
      containerId: context.props.containerId,
      controller: context,
      editorSession: context.editorSession,
      surfaceManager: context.surfaceManager,
      user: context.props.user,
    }
    return new TrackChangesProvider(context.doc, config)
  }

  /*

    HANDLERS

  */

  handleTransaction(options) {
    const extendedOptions = cloneDeep(options)
    // options.editor = this.config.controller
    extendedOptions.editorSession = this.config.editorSession
    extendedOptions.selection = this.getSelection()
    extendedOptions.surface = this.getActiveSurface()
    extendedOptions.user = this.config.user
    this.chooseHanlder(extendedOptions)
  }

  chooseHanlder(options) {
    if (options.status === 'add') return this.handleAdd(options)
    if (options.status === 'delete') return this.handleDelete(options)
    if (options.status === 'enter') return this.handleEnter(options)
    if (options.status === 'paste') return this.handlePaste(options)
    return this.extraCases(options)
  }

  handleAdd(options) {
    const isCollapsed = isSelectionCollapsed(options)
    isCollapsed
      ? this.handleAddCollapsed(options)
      : this.handleAddNonCollapsed(options)
    return false
  }

  handleDelete(options) {
    const { key, move, selection, event, parentFunc } = options
    const isCollapsed = isSelectionCollapsed(options)

    options.direction = {
      cursorTo: move === 'left' ? 'start' : 'end',
      key,
      move,
    }

    // handle notes
    this.updateNotesAsDeleted()

    // handle deleted paragraph for now
    if (selection.start.offset === 0 && selection.end.offset === 0) {
      parentFunc(event)
      return false
    }

    isCollapsed
      ? this.handleDeleteCollapsed(options)
      : this.handleDeleteNonCollapsed(options)

    return false
  }

  handleEnter(options) {
    const isCollapsed = isSelectionCollapsed(options)
    isCollapsed
      ? this.handleEnterCollapsed(options)
      : this.handleEnterNonCollapsed(options)

    return false
  }

  handlePaste(options) {
    const isCollapsed = isSelectionCollapsed(options)
    isCollapsed
      ? this.handlePasteCollapsed(options)
      : this.handlePasteNonCollapsed(options)

    return false
  }

  // handleAddCollapsed DONE except CHECK TODO CHECK NOT FROM SAME USER
  handleAddCollapsed(options) {
    const notOnTrack = isNotOnTrackAnnotation(options)
    const isOnAdd = isOnAnnotation(options, 'add')
    const isOnDelete = isOnAnnotation(options, 'delete')

    if (notOnTrack) return this.insertFirstAnnotatedCharacter(options)

    if (isOnDelete) {
      const annotation = getAnnotationByStatus(options, 'delete')

      const isOnLeftEdge = isSelectionOnLeftEdge(options, annotation)
      const isOnRightEdge = isSelectionOnRightEdge(options, annotation)
      const withinAnnotation = isSelectionContainedWithin(
        options,
        annotation,
        true,
      )

      // if contained within the delete annotation, move it to the edge,
      // insert character, set event to null so that the character does
      // not get inserted twice, and handle again
      if (withinAnnotation) {
        moveCursorTo(options, annotation.end.offset)
        options.selection = this.getSelection()
        return this.handleAdd(options)
      }

      if (isOnLeftEdge) return this.insertFirstAnnotatedCharacter(options)

      if (isOnRightEdge) {
        TrackChange.autoExpandRight = false
        return this.insertFirstAnnotatedCharacter(options)
      }
    }

    if (isOnAdd) {
      // annotation gets expanded automatically, unless the selection is on its left edge
      const annotation = getAnnotationByStatus(options, 'add')
      const isOnLeftEdge = isSelectionOnLeftEdge(options, annotation)
      const isOnRightEdge = isSelectionOnRightEdge(options, annotation)
      const isFromSameUser = isAnnotationFromTheSameUser(options, annotation)
      const mode = this.getMode()

      if (isFromSameUser) {
        isOnLeftEdge
          ? this.expandAddAnnotationLeft(options, annotation)
          : insertText(options)
      }
      // TODO CHECK NOT FROM SAME USER
      if (!isFromSameUser) {
        if (isOnRightEdge) {
          insertCharacterWithoutExpandingAnnotation(options, annotation)
        } else {
          insertText(options)
        }
        if (mode) createAdditionAnnotationOnLastChar(options)
      }
      return false
    }
    return false
  }

  handleAddNonCollapsed(options) {
    let { selection } = options

    const notOnTrack = isNotOnTrackAnnotation(options)
    const isOnDelete = isOnAnnotation(options, 'delete')

    if (notOnTrack) return this.deleteSelectedAndCreateAddition(options)

    // delete all additions of the same user and
    // shorten selection by the number of deleted characters
    const shortenBy = deleteAllOwnAdditions(options, selection)
    const startOffset = selection.start.offset
    const endOffset = selection.end.offset - shortenBy
    selection = updateSelection(options, selection, startOffset, endOffset)
    options.selection = selection

    if (isOnDelete) {
      const annotation = getAnnotationByStatus(options, 'delete')
      const withinAnnotation = isSelectionContainedWithin(options, annotation)

      if (withinAnnotation) {
        // if selection is wholly contained within a delete annotation,
        // move to the end of the annotation and handle again
        moveCursorTo(options, annotation.end.offset)
        options.selection = this.getSelection()
        return this.handleAddCollapsed(options)
      }
    }

    // after deleting all own additions, there is still text selected
    // mark it as deleted and add new addition annotation at the end
    // TODO -- use selection.isCollapsed()
    if (selection.end.offset > selection.start.offset) {
      this.deleteSelectedAndCreateAddition(options)
      return false
    }

    // if you got here, deleting all own additions left a collapsed selection
    // (ie. the whole selection was on an own addition annotation)
    // since selection is collapsed, handle it as such
    return this.handleAddCollapsed(options)
  }

  handleDeleteCollapsed(options) {
    const { direction } = options

    const notOnTrack = isNotOnTrackAnnotation(options)
    const isOnAdd = isOnAnnotation(options, 'add')
    const isOnDelete = isOnAnnotation(options, 'delete')

    // DONE
    if (notOnTrack) return this.insertFirstDeletedCharacter(options)

    if (isOnAdd) {
      const annotation = getAnnotationByStatus(options, 'add')
      const isOnLeftEdge = isSelectionOnLeftEdge(options, annotation)
      const isOnRightEdge = isSelectionOnRightEdge(options, annotation)
      const isFromSameUser = isAnnotationFromTheSameUser(options, annotation)
      const mode = this.getMode()
      const key = direction.key

      // use this variable to throw the flow to the isOnDelete handler underneath
      let pass = false

      // when on own additions, simply delete the character
      // unless it is on the edge: then make a deletion annotation

      // TODO -- watch it for different users
      // TODO CHECK WHAT IT DOES
      if (
        (isOnLeftEdge && key === 'BACKSPACE') ||
        (isOnRightEdge && key === 'DELETE') ||
        (!isFromSameUser && !isOnDelete)
      ) {
        if (mode) return selectCharacterAndMarkDeleted(options)
        pass = true
      }

      if (!isFromSameUser && isOnDelete) pass = true
      if (!pass) return deleteCharacter(options)
    }

    if (isOnDelete) {
      const annotation = getAnnotationByStatus(options, 'delete')
      const isOnLeftEdge = isSelectionOnLeftEdge(options, annotation)
      const isOnRightEdge = isSelectionOnRightEdge(options, annotation)
      const isFromSameUser = isAnnotationFromTheSameUser(options, annotation)
      const key = direction.key

      let moveOnly
      let point

      if (!isOnLeftEdge && !isOnRightEdge) {
        point = annotation[direction.cursorTo + 'Offset']
        moveOnly = true
      } else if (isOnLeftEdge && key === 'DELETE') {
        point = annotation.end.offset
        moveOnly = true
      } else if (isOnRightEdge && key === 'BACKSPACE') {
        point = annotation.start.offset
        moveOnly = true
      }

      if (moveOnly) {
        return moveCursorTo(options, point)
      }

      if (isFromSameUser) {
        options.cursorTo = direction.cursorTo
        return this.expandDeleteAnnotation(options, annotation)
      } else {
        return selectCharacterAndMarkDeleted(options)
      }
    }
    return false
  }

  handleDeleteNonCollapsed(options) {
    const { direction } = options
    // let { selection } = options

    const notOnTrack = isNotOnTrackAnnotation(options)
    const isOnDelete = isOnAnnotation(options, 'delete')

    if (notOnTrack) return this.markNonCollapsedSelectionAsDeleted(options)

    if (isOnDelete) {
      const annotation = getAnnotationByStatus(options, 'delete')
      const containedWithin = isSelectionContainedWithin(options, annotation)

      if (containedWithin) {
        const point = annotation[`${direction.cursorTo}Offset`]
        return moveCursorTo(options, point)
      }
    }

    this.markNonCollapsedSelectionAsDeleted(options)

    return false
  }

  handleEnterCollapsed(options) {
    const { event, parentFunc } = options
    return parentFunc(event)
  }

  handleEnterNonCollapsed(options) {
    const { selection } = options
    if (selection.isContainerSelection()) {
      this.markSelectionAsDeletedForEnterContanier(options)
    } else {
      this.markSelectionAsDeletedForEnter(options)
    }
  }

  extraCases(options) {
    // TO BE DEFINED IF ANY
  }

  updateNotesAsDeleted(selection = null) {
    const initialSelection =
      selection === null ? this.editorSession.getSelection() : selection

    const doc = this.editorSession.getDocument()
    const rootEl = this.config.controller

    const notes = documentHelpers.getPropertyAnnotationsForSelection(
      doc,
      initialSelection,
      { type: 'note' },
    )

    if (notes.length > 0) {
      forEach(notes, note => {
        note.disabled = 'true'
      })
      rootEl.rerender()
    }
  }

  // Inline Formating

  trackCreatedFormatingInText(node) {
    const user = this.config.user
    const selection = this.editorSession.getSelection()
    const trackFormats = documentHelpers.getPropertyAnnotationsForSelection(
      this.editorSession.getDocument(),
      selection,
      { type: 'track-change-format' },
    )

    if (trackFormats.length >= 1) {
      if (this.checkIfAddFormatFromSameUser(node, trackFormats)) return
    }

    const obj = {
      username: user.username,
      type: node[0].type,
      formatId: node[0].id,
    }
    const info = { hidden: true }

    setTimeout(() => {
      this.editorSession.transaction(tx => {
        tx.create({
          selection,
          status: 'add-formating',
          type: 'track-change-format',
          addedType: [obj],
          path: selection.path,
          start: selection.start,
          end: selection.end,
          user,
        })
      }, info)
    })
  }

  trackDeletedFormatingInText(node) {
    const user = this.config.user
    const selection = this.editorSession.getSelection()
    const trackFormats = documentHelpers.getPropertyAnnotationsForSelection(
      this.editorSession.getDocument(),
      selection,
      { type: 'track-change-format' },
    )

    if (trackFormats.length >= 1) {
      if (this.checkIfDeleteFormatFromSameUser(node, trackFormats)) return
    }

    const obj = {
      username: user.username,
      type: node[0].type,
      formatId: node[0].id,
    }
    const info = { hidden: true }

    this.editorSession.transaction(tx => {
      tx.create({
        selection,
        status: 'delete-formating',
        type: 'track-change-format',
        oldType: [obj],
        path: selection.path,
        start: node[0].start,
        end: node[0].end,
        user,
      })
    }, info)
  }

  trackExpandedFormatingInText() {
    const selection = this.editorSession.getSelection()
    const trackFormat = documentHelpers.getPropertyAnnotationsForSelection(
      this.editorSession.getDocument(),
      selection,
      { type: 'track-change-format' },
    )
    if (trackFormat.length === 0) return false

    this.editorSession.transaction(tx => {
      annotationHelpers.expandAnnotation(tx, trackFormat[0], selection)
    })
  }

  checkIfAddFormatFromSameUser(node, trackFormats) {
    let flag = false
    const start = node[0].start ? node[0].start.offset : node[0].startOffset
    const end = node[0].end ? node[0].end.offset : node[0].endOffset
    const user = this.config.user
    forEach(trackFormats, trackFormat => {
      const equalAnno =
        trackFormat.start.offset === start && trackFormat.end.offset === end
      if (trackFormat.oldType.length === 0) return
      if (
        trackFormat.oldType[0].username === user.username &&
        trackFormat.oldType[0].type === node[0].type &&
        equalAnno
      ) {
        flag = true
        const info = { hidden: true }
        this.editorSession.transaction(tx => {
          tx.delete(trackFormat.id)
        }, info)
      }
    })
    return flag
  }

  checkIfDeleteFormatFromSameUser(node, trackFormats) {
    let flag = false
    const user = this.config.user
    forEach(trackFormats, trackFormat => {
      if (trackFormat.addedType.length === 0) return
      if (
        trackFormat.addedType[0].username === user.username &&
        trackFormat.addedType[0].type === node[0].type
      ) {
        flag = true
        const info = { hidden: true }

        setTimeout(() => {
          this.editorSession.transaction(tx => {
            tx.delete(trackFormat.id)
          }, info)
        })
      }
    })
    return flag
  }

  // Block level Formating

  // MOVE TO HELPERS (A LOT OF DUPLICATE CASES --CLEANUP--)
  insertFirstAnnotatedCharacter(options) {
    const { user, selection, surface } = options
    let addedCharacter = options.event.data
    //Space handlder
    if (typeof addedCharacter === 'undefined') {
      addedCharacter = ' '
    }
    const containerId = surface.containerId
    let sel = ''

    this.editorSession.transaction(tx => {
      tx.insertText(addedCharacter)

      sel = tx.createSelection({
        type: 'property',
        containerId,
        surfaceId: containerId,
        path: selection.path,
        startOffset: selection.start.offset,
        endOffset: selection.end.offset,
      })

      tx.create({
        status: 'add',
        type: 'track-change',
        containerId,
        surfaceId: containerId,
        path: sel.path,
        startOffset: sel.start.offset,
        endOffset: sel.end.offset + 1,
        user: {
          id: user.id,
          username: user.username,
        },
      })
    })
  }

  insertFirstDeletedCharacter(options) {
    const { user, selection, surface, status, move } = options
    let newSel = ''
    const containerId = surface.containerId

    const pointerStart =
      move === 'left' ? selection.start.offset - 1 : selection.start.offset

    const pointerEnd =
      move === 'left' ? selection.end.offset : selection.end.offset + 1

    const createSelectionPointerStart =
      move === 'left' ? selection.start.offset - 1 : selection.start.offset + 1

    const createSelectionPointerEnd =
      move === 'left' ? selection.start.offset - 1 : selection.start.offset + 1

    this.editorSession.transaction(tx => {
      tx.setSelection({
        type: 'property',
        containerId,
        surfaceId: containerId,
        path: selection.path,
        startOffset: pointerStart,
        endOffset: pointerEnd,
      })

      tx.create({
        status,
        type: 'track-change',
        containerId,
        surfaceId: containerId,
        path: selection.path,
        startOffset: pointerStart,
        endOffset: pointerEnd,
        user: {
          id: user.id,
          username: user.username,
        },
      })

      tx.setSelection({
        type: 'property',
        containerId,
        surfaceId: containerId,
        path: selection.path,
        startOffset: createSelectionPointerStart,
        endOffset: createSelectionPointerEnd,
      })
      //Hack in order to not refresh surface and show deletiion
      tx.insertText('')
    })
  }

  expandDeleteAnnotation(options, annotation) {
    const { selection, surface, move } = options
    const containerId = surface.containerId
    let sel = ''

    const pointer =
      move === 'left' ? selection.start.offset - 1 : selection.start.offset + 1

    this.editorSession.transaction(tx => {
      sel = tx.createSelection({
        type: 'property',
        containerId,
        surfaceId: containerId,
        path: selection.path,
        startOffset: pointer,
        endOffset: pointer,
      })
      return annotationHelpers.expandAnnotation(tx, annotation, sel)
    })
    this.editorSession.setSelection(sel)
  }

  expandAddAnnotationLeft(options, annotation) {
    const { selection } = options

    const addedCharacter = options.event.data

    this.editorSession.transaction(tx => {
      tx.insertText(addedCharacter)

      const annoCopy = cloneDeep(annotation)
      annoCopy.start.offset += 1
      annotationHelpers.expandAnnotation(tx, annoCopy, selection)
    })
  }

  deleteSelectedAndCreateAdditionContanier(options) {
    this.config.controller.emit('displayNotification', () => {
      return 'Multi block delete and then add operations are not yet supported.'
    })
  }

  deleteSelectedAndCreateAddition(options) {
    const { selection, user, surface } = options
    if (selection.isContainerSelection()) {
      this.deleteSelectedAndCreateAdditionContanier(options)
    } else {
      const additions = getAllAnnotationsByStatus(options, 'add')
      const deletions = getAllAnnotationsByStatus(options, 'delete')

      const ownAdditions = filter(additions, annotation => {
        return isAnnotationFromTheSameUser(options, annotation)
      })

      const ownDeletions = filter(deletions, annotation => {
        return isAnnotationFromTheSameUser(options, annotation)
      })

      const containerId = surface.containerId
      let diff = 0
      let allDiff = 0

      this.editorSession.transaction(tx => {
        if (ownAdditions.length > 0) {
          forEach(ownAdditions, (addition, index) => {
            const previous = ownAdditions[index - 1]
            if (previous) diff += previous.end.offset - previous.start.offset
            allDiff += addition.end.offset - addition.start.offset

            tx.setSelection({
              type: 'property',
              path: addition.path,
              surfaceId: containerId,
              startOffset: addition.start.offset - diff,
              endOffset: addition.end.offset - diff,
            })
            tx.deleteSelection()
          })
        }

        if (ownDeletions.length > 0) {
          forEach(ownDeletions, (deletion, index) => {
            tx.delete(deletion.id)
          })
        }

        tx.create({
          status: 'delete',
          type: 'track-change',
          containerId,
          surfaceId: containerId,
          path: selection.path,
          startOffset: selection.start.offset,
          endOffset: selection.end.offset - allDiff,
          user: {
            id: user.id,
            username: user.username,
          },
        })
        tx.setSelection({
          type: 'property',
          containerId,
          surfaceId: containerId,
          path: selection.path,
          startOffset: selection.end.offset,
          endOffset: selection.end.offset,
        })

        TrackChange.autoExpandRight = false

        let addedText = options.event.data
        //Handle Space
        if (typeof addedText === 'undefined') {
          addedText = ' '
        }
        tx.insertText(addedText)

        tx.create({
          status: 'add',
          type: 'track-change',
          containerId,
          surfaceId: containerId,
          path: selection.path,
          startOffset: selection.end.offset,
          endOffset: selection.end.offset + 1,
          user: {
            id: user.id,
            username: user.username,
          },
        })
      })
    }
  }

  markNonCollapsedSelectionAsDeletedContainer(options) {
    this.config.controller.emit('displayNotification', () => {
      return 'Multi block delete operations are not yet supported.'
    })
  }

  markNonCollapsedSelectionAsDeleted(options) {
    const { selection, user, surface } = options
    if (selection.isContainerSelection()) {
      this.markNonCollapsedSelectionAsDeletedContainer(options)
    } else {
      const additions = getAllAnnotationsByStatus(options, 'add')
      const deletions = getAllAnnotationsByStatus(options, 'delete')

      const ownAdditions = filter(additions, annotation => {
        return isAnnotationFromTheSameUser(options, annotation)
      })

      const ownDeletions = filter(deletions, annotation => {
        return isAnnotationFromTheSameUser(options, annotation)
      })

      const containerId = surface.containerId
      let diff = 0
      let allDiff = 0
      let newSel = ''
      this.editorSession.transaction(tx => {
        if (ownAdditions.length > 0) {
          forEach(ownAdditions, (addition, index) => {
            const previous = ownAdditions[index - 1]
            if (previous) diff += previous.end.offset - previous.start.offset
            allDiff += addition.end.offset - addition.start.offset

            tx.setSelection({
              type: 'property',
              path: addition.path,
              surfaceId: containerId,
              startOffset: addition.start.offset - diff,
              endOffset: addition.end.offset - diff,
            })
            tx.deleteSelection()
          })
        }

        if (ownDeletions.length > 0) {
          forEach(ownDeletions, (deletion, index) => {
            tx.delete(deletion.id)
          })
        }

        tx.create({
          status: 'delete',
          type: 'track-change',
          containerId,
          surfaceId: containerId,
          path: selection.path,
          startOffset: selection.start.offset,
          endOffset: selection.end.offset - allDiff,
          user: {
            id: user.id,
            username: user.username,
          },
        })
        tx.setSelection({
          type: 'property',
          containerId,
          surfaceId: containerId,
          path: selection.path,
          startOffset: selection.start.offset,
          endOffset: selection.start.offset,
        })
        //HACK in order to not refresh surface and show the deletion
        tx.insertText('')
      })
    }
  }

  markSelectionAsDeletedForEnter(options) {
    const { selection, user, surface } = options
    const additions = getAllAnnotationsByStatus(options, 'add')
    const deletions = getAllAnnotationsByStatus(options, 'delete')

    const ownAdditions = filter(additions, annotation => {
      return isAnnotationFromTheSameUser(options, annotation)
    })

    const ownDeletions = filter(deletions, annotation => {
      return isAnnotationFromTheSameUser(options, annotation)
    })

    const containerId = surface.containerId
    let diff = 0
    let allDiff = 0

    this.editorSession.transaction(tx => {
      if (ownAdditions.length > 0) {
        forEach(ownAdditions, (addition, index) => {
          const previous = ownAdditions[index - 1]
          if (previous) diff += previous.end.offset - previous.start.offset
          allDiff += addition.end.offset - addition.start.offset

          tx.setSelection({
            type: 'property',
            path: addition.path,
            surfaceId: containerId,
            startOffset: addition.start.offset - diff,
            endOffset: addition.end.offset - diff,
          })
          tx.deleteSelection()
        })
      }

      if (ownDeletions.length > 0) {
        forEach(ownDeletions, (deletion, index) => {
          tx.delete(deletion.id)
        })
      }

      tx.create({
        status: 'delete',
        type: 'track-change',
        containerId,
        surfaceId: containerId,
        path: selection.path,
        startOffset: selection.start.offset,
        endOffset: selection.end.offset - allDiff,
        user: {
          id: user.id,
          username: user.username,
        },
      })
      tx.setSelection({
        type: 'property',
        containerId,
        surfaceId: containerId,
        path: selection.path,
        startOffset: selection.end.offset - allDiff,
        endOffset: selection.end.offset - allDiff,
      })
      tx.break()
    })
  }
  markSelectionAsDeletedForEnterContanier(options) {
    const { selection, user, surface } = options
    const allDiff = 0
    const containerId = surface.containerId
    const properties = selection.splitIntoPropertySelections()

    this.editorSession.transaction(tx => {
      forEach(properties, property => {
        tx.create({
          status: 'delete',
          type: 'track-change',
          containerId,
          surfaceId: containerId,
          path: property.path,
          startOffset: property.start.offset,
          endOffset: property.end.offset - allDiff,
          user: {
            id: user.id,
            username: user.username,
          },
        })
        tx.setSelection({
          type: 'property',
          containerId,
          surfaceId: containerId,
          path: property.path,
          startOffset: property.end.offset - allDiff,
          endOffset: property.end.offset - allDiff,
        })
      })
      tx.break()
    })
    surface.rerender()
  }

  handlePasteCollapsed(options) {
    const { content } = options

    if (content.data.nodes['text-snippet']) {
      this.createTextSnippet(options)
    } else {
      this.createSnippet(options)
    }
  }

  handlePasteNonCollapsed(options) {
    this.config.controller.emit('displayNotification', () => {
      return 'Not yet supported. Delete selected text and paste.'
    })
  }

  handleCutContainer(options) {
    this.config.controller.emit('displayNotification', () => {
      return 'Multiblock cut operations are not yet supported.'
    })
  }

  handleCut(options) {
    options.editorSession = this.editorSession
    options.user = this.config.user
    if (this.editorSession.getSelection().isContainerSelection()) {
      this.handleCutContainer(options)
    } else {
      const additions = getAllAnnotationsByStatus(options, 'add')
      const deletions = getAllAnnotationsByStatus(options, 'delete')

      const user = this.config.user
      const surface = this.getActiveSurface()

      // if we need to filter them

      // const ownAdditions = filter(additions, annotation => {
      //   return isAnnotationFromTheSameUser(options, annotation)
      // })
      //
      // const ownDeletions = filter(deletions, annotation => {
      //   return isAnnotationFromTheSameUser(options, annotation)
      // })

      const ownAdditions = []
      const ownDeletions = []

      const selection = this.editorSession.getSelection()

      const containerId = surface.containerId

      let diff = 0
      let allDiff = 0
      let newSel = ''

      this.editorSession.transaction(tx => {
        if (ownAdditions.length > 0) {
          forEach(ownAdditions, (addition, index) => {
            const previous = ownAdditions[index - 1]
            if (previous) diff += previous.end.offset - previous.start.offset
            allDiff += addition.end.offset - addition.start.offset

            tx.setSelection({
              type: 'property',
              path: addition.path,
              surfaceId: containerId,
              startOffset: addition.start.offset - diff,
              endOffset: addition.end.offset - diff,
            })
            tx.deleteSelection()
          })
        }

        if (ownDeletions.length > 0) {
          forEach(ownDeletions, (deletion, index) => {
            tx.delete(deletion.id)
          })
        }

        tx.create({
          status: 'delete',
          type: 'track-change',
          containerId,
          surfaceId: containerId,
          path: selection.path,
          startOffset: selection.start.offset,
          endOffset: selection.end.offset - allDiff,
          user: {
            id: user.id,
            username: user.username,
          },
        })
        tx.setSelection({
          type: 'property',
          containerId,
          surfaceId: containerId,
          path: selection.path,
          startOffset: selection.start.offset,
          endOffset: selection.start.offset,
        })
        //HACK in order to not refresh surface and show the deletion
        tx.insertText('')

        //Set Selection to all
        tx.setSelection({
          type: 'property',
          containerId,
          surfaceId: containerId,
          path: selection.path,
          startOffset: selection.start.offset,
          endOffset: selection.end.offset,
        })
      })
    }
  }

  createSnippet(options) {
    const { content, user, SubstanceClipboard } = options
    let selection = this.editorSession.getSelection()
    const surface = this.getActiveSurface()
    const containerId = surface.containerId
    const { nodes } = content.data.nodes['snippet']
    const firstNode = content.get(nodes[0])
    let lengthFirst = 0

    if (firstNode.type !== 'list') lengthFirst = firstNode.getText().length
    const info = { hidden: true }
    let length = 0

    const trackChange = documentHelpers.getPropertyAnnotationsForSelection(
      this.editorSession.document,
      selection,
      { type: 'track-change' },
    )

    if (trackChange.length > 0) {
      this.editorSession.transaction(tx => {
        selection = tx.createSelection({
          type: 'property',
          containerId: containerId,
          surfaceId: containerId,
          path: trackChange[0].path,
          startOffset: trackChange[0].endOffset,
          endOffset: trackChange[0].endOffset,
        })
      }, info)
      this.editorSession.setSelection(selection)
      TrackChange.autoExpandRight = false
    }

    if (SubstanceClipboard) {
      this.editorSession.transaction(
        tx => {
          tx.paste(content)

          // first node
          if (firstNode.type !== 'list') {
            tx.create({
              status: 'add',
              type: 'track-change',
              containerId,
              surfaceId: containerId,
              path: selection.path,
              startOffset: selection.start.offset,
              endOffset: selection.start.offset + lengthFirst,
              user: {
                id: user.id,
                username: user.username,
              },
            })
          } else {
            forEach(firstNode.items, item => {
              const listItem = content.get(item)
              tx.setSelection({
                type: 'property',
                containerId,
                surfaceId: containerId,
                path: [item, 'content'],
                startOffset: 0,
                endOffset: 0,
              })
              tx.insertText('')

              tx.create({
                status: 'add',
                type: 'track-change',
                containerId,
                surfaceId: containerId,
                path: [item, 'content'],
                startOffset: 0,
                endOffset: listItem.content.length,
                user: {
                  id: user.id,
                  username: user.username,
                },
              })
            })
          }

          forEach(Object.keys(content.data.nodes), key => {
            if (AcceptedNodeTypes.includes(content.data.nodes[key].type)) {
              if (content.get(key)) {
                length = content.get(key).getText().length
                tx.create({
                  status: 'add',
                  type: 'track-change',
                  containerId,
                  surfaceId: containerId,
                  path: [content.get(key).id, 'content'],
                  startOffset: 0,
                  endOffset: length,
                  user: {
                    id: user.id,
                    username: user.username,
                  },
                })
              }
            }
          })
        },
        { action: 'paste' },
      )

      //lists
      forEach(Object.keys(content.data.nodes), key => {
        if (content.data.nodes[key].type === 'list') {
          const list = this.editorSession.document.get(
            content.data.nodes[key].id,
          )
          this.editorSession.transaction(tx => {
            forEach(list.items, item => {
              const listItem = this.editorSession.document.get(item)
              tx.setSelection({
                type: 'property',
                containerId,
                surfaceId: containerId,
                path: [item, 'content'],
                startOffset: 0,
                endOffset: 0,
              })
              tx.insertText('')

              tx.create({
                status: 'add',
                type: 'track-change',
                containerId,
                surfaceId: containerId,
                path: [item, 'content'],
                startOffset: 0,
                endOffset: listItem.content.length,
                user: {
                  id: user.id,
                  username: user.username,
                },
              })
            })
          }, info)
        }
      })

      //remove duplicates
      this.editorSession.transaction(tx => {
        const sel = this.editorSession.createSelection({
          type: 'property',
          containerId,
          surfaceId: containerId,
          path: [firstNode.id, 'content'],
          startOffset: 0,
          endOffset: lengthFirst,
        })
        const trackChange = documentHelpers.getPropertyAnnotationsForSelection(
          this.editorSession.getDocument(),
          sel,
          { type: 'track-change' },
        )
        tx.delete(trackChange[0].id)
      }, info)
      return
    }

    //paste from external source
    this.editorSession.transaction(
      tx => {
        tx.paste(content)
        tx.create({
          status: 'add',
          type: 'track-change',
          containerId,
          surfaceId: containerId,
          path: selection.path,
          startOffset: selection.start.offset,
          endOffset: selection.start.offset + lengthFirst,
          user: {
            id: user.id,
            username: user.username,
          },
        })

        forEach(content.data.nodes, node => {
          if (node.type === 'list') {
            forEach(node.items, item => {
              const listItem = content.get(item)
              tx.create({
                status: 'add',
                type: 'track-change',
                containerId,
                surfaceId: containerId,
                path: [item, 'content'],
                startOffset: 0,
                endOffset: listItem.content.length,
                user: {
                  id: user.id,
                  username: user.username,
                },
              })
            })
          }
          if (AcceptedNodeTypes.includes(node.type)) {
            if (this.editorSession.document.get(node.id)) return
            if (content.get(node.id)) {
              length = content.get(node.id).getText().length
              tx.create({
                status: 'add',
                type: 'track-change',
                containerId,
                surfaceId: containerId,
                path: [node.id, 'content'],
                startOffset: 0,
                endOffset: length,
                user: {
                  id: user.id,
                  username: user.username,
                },
              })
            }
          }
        })
      },
      { action: 'paste' },
    )
  }

  createTextSnippet(options) {
    const { content, user } = options
    let selection = this.editorSession.getSelection()
    const doc = this.editorSession.document
    const surface = this.getActiveSurface()
    const containerId = surface.containerId
    const info = { hidden: true }

    const trackChange = documentHelpers.getPropertyAnnotationsForSelection(
      doc,
      selection,
      { type: 'track-change' },
    )

    if (trackChange.length > 0) {
      this.editorSession.transaction(tx => {
        selection = tx.createSelection({
          type: 'property',
          containerId: containerId,
          surfaceId: containerId,
          path: trackChange[0].path,
          startOffset: trackChange[0].endOffset,
          endOffset: trackChange[0].endOffset,
        })
      }, info)
      this.editorSession.setSelection(selection)
      TrackChange.autoExpandRight = false
    }

    const length = content.data.nodes['text-snippet'].getText().length

    this.editorSession.transaction(
      tx => {
        tx.paste(content)

        tx.create({
          status: 'add',
          type: 'track-change',
          containerId,
          surfaceId: containerId,
          path: selection.path,
          startOffset: selection.start.offset,
          endOffset: selection.end.offset + length,
          user: {
            id: user.id,
            username: user.username,
          },
        })
      },
      { action: 'paste' },
    )
  }

  /*

    ACCEPT / REJECT

  */

  resolve(annotations, action) {
    const surface = this.getActiveSurface()
    const containerId = surface.containerId
    const toBeRemoved = []
    const keepAnnotatios = []
    const formatAdded = []
    const formatDeleted = []

    this.editorSession.transaction(tx => {
      forEach(annotations, (annotation, index) => {
        const { status, type } = annotation
        if (type === 'track-change') {
          if (
            (action === 'accept' && status === 'delete') ||
            (action === 'reject' && status === 'add')
          ) {
            toBeRemoved.push(annotation)
          } else {
            keepAnnotatios.push(annotation)
          }
          tx.delete(annotation.id)
        } else {
          tx.delete(annotation.id)
          if (action === 'reject' && status === 'add-formating') {
            tx.delete(annotation.addedType[0].formatId)
          } else if (action === 'reject' && status === 'delete-formating') {
            tx.create({
              type: annotation.oldType[0].type,
              containerId,
              surfaceId: containerId,
              path: annotation.path,
              start: annotation.start,
              end: annotation.end,
            })
          }
        }
      })

      // check if notes to rerender note surface, to remove deletion
      forEach(keepAnnotatios, keepAnno => {
        if (keepAnno.status === 'delete') {
          const notes = documentHelpers.getPropertyAnnotationsForSelection(
            this.editorSession.getDocument(),
            keepAnno.getSelection(),
            { type: 'note' },
          )
          if (notes.length >= 1) {
            forEach(notes, note => {
              note.disabled = false
            })
            this.rerenderEditor = true
          }
        }
      })

      eachRight(toBeRemoved, (anno, index) => {
        tx.setSelection({
          type: 'property',
          path: anno.path,
          containerId,
          surfaceId: containerId,
          startOffset: anno.start.offset,
          endOffset: anno.end.offset,
        })
        tx.deleteSelection()
      })
    })
    // update ui for comments
    const editor = this.config.controller
    editor.emit('ui:updated')

    // Update editor in note exists
    if (this.rerenderEditor) {
      editor.rerender()
      this.rerenderEditor = false
    }
  }

  /*
OTHER STUFF
*/

  getSelection() {
    return this.editorSession.getSelection()
  }

  getMode() {
    const { commandStates } = this.config.commandManager
    const trackState = commandStates['track-change']
    return trackState.mode
  }

  focus(annotation) {
    if (!annotation) return

    this.editorSession.transaction(tx => {
      tx.setSelection({
        type: 'property',
        containerId: annotation.containerId,
        surfaceId: annotation.containerId,
        path: annotation.node.path,
        startOffset: annotation.node.start.offset,
        endOffset: annotation.node.start.offset,
      })
    })
  }

  canAct() {
    const editorProps = this.config.controller.props
    const { mode } = editorProps
    const canToggleTrackChanges = mode.trackChanges.toggle
    return canToggleTrackChanges
  }
}

export default TrackChangesProvider
