import { Tool } from 'substance'

class TrackChangePreviousControlTool extends Tool {
  getClassNames() {
    return 'track-change-previous'
  }

  renderButton($$) {
    const el = super.renderButton($$)
    const iconPrevious = $$('span').addClass('track-changes-icon-previous')
    const seperator = $$('span').addClass(
      'toolbar-track-changes-previous-seperator',
    )
    el.append(iconPrevious, 'Previous', seperator)

    return el
  }
}

TrackChangePreviousControlTool.type = 'track-change-next'

export default TrackChangePreviousControlTool
