export default {
  type: 'track-change-format',
  tagName: 'track-change-format',

  import: function(element, node, converter) {
    node.status = element.attr('status')
    node.oldType = JSON.parse(element.attr('oldType'))
    node.addedType = JSON.parse(element.attr('addedType'))
    node.user = {
      id: element.attr('user-id'),
      username: element.attr('username'),
    }
  },

  export: function(node, element, converter) {
    const { status, user, oldType, addedType } = node

    element.setAttribute('status', status)
    element.setAttribute('oldType', JSON.stringify(oldType))
    element.setAttribute('addedType', JSON.stringify(addedType))
    element.setAttribute('user-id', user.id)
    element.setAttribute('username', user.username)
  },
}
