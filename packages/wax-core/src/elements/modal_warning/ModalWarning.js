import { each } from 'lodash'
import Modal from '../modal/Modal'

class ModalWarning extends Modal {
  constructor(...props) {
    super(...props)

    // TODO -- use state?
    this.save = false
  }

  render($$) {
    const el = $$('div').addClass('sc-modal')

    const modalHeader = $$('div')
      .addClass('sc-modal-header')
      .append('There are unsaved changes. Do you want to save your work?')

    const saveExit = $$('button')
      .addClass('sc-modal-button')
      .addClass('sc-modal-button-save-exit')
      .append('Save and quit')
      .on('click', this.saveExitWriter)

    const exit = $$('button')
      .addClass('sc-modal-button')
      .addClass('sc-modal-button-exit')
      .append('Quit without saving')
      .on('click', this.exitWriter)

    const cancelMessage = $$('span').append('Take me back to the editor')

    const cancel = $$('button')
      .addClass('sc-modal-button')
      .addClass('sc-modal-button-close')
      .append(cancelMessage)
      .on('click', this.closeModal)

    const modalActionsContainer = $$('div')
      .addClass('sc-modal-actions')
      .append(saveExit, exit, cancel)

    if (this.props.width) {
      el.addClass(`sm-width-${this.props.width}`)
    }

    el.append(
      $$('div')
        .addClass('se-body')
        .attr('data-clickable', true)
        .append(modalHeader)
        .append(modalActionsContainer),
    )
    return el
  }

  closeModal() {
    this.context.editor.emit('toggleModal', 'changesNotSaved')
  }

  setSelectionOnStartOfText() {
    const doc = this.context.editorSession.getDocument()
    const nodes = doc.getNodes()
    const editorContainerId = this.context.editor.props.containerId
    const firstParagraph = nodes[editorContainerId].nodes[0]

    this.context.editorSession.transaction(tx => {
      tx.setSelection({
        type: 'property',
        containerId: editorContainerId,
        surfaceId: editorContainerId,
        path: [firstParagraph, 'content'],
        startOffset: 0,
        endOffset: 0,
      })
    })
  }

  editorSessionSave() {
    if (!this.save) {
      this.setSelectionOnStartOfText()
      // TODO -- there must be a function for this
      each(this.context.editorSession._history.doneChanges, key => {
        this.context.editorSession.undo()
      })
    }

    return new Promise((resolve, reject) => {
      resolve(this.context.editorSession.save())
    })
  }

  saveExitWriter() {
    this.save = true
    this.goToLocation()
  }

  exitWriter() {
    this.goToLocation()
  }

  goToLocation() {
    const { action, location, onClose } = this.props

    this.editorSessionSave().then(() => {
      onClose(location, action)
    })
  }
}

export default ModalWarning
