import { TextBlock } from 'substance'

class ExtractPoetry extends TextBlock {}

ExtractPoetry.type = 'extract-poetry'

export default ExtractPoetry
