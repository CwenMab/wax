import ExtractPoetry from './ExtractPoetry'
import ExtractPoetryComponent from './ExtractPoetryComponent'
import ExtractPoetryHTMLConverter from './ExtractPoetryHTMLConverter'
import WaxSwitchTextTypeCommand from '../../commands/WaxSwitchTextTypeCommand'

export default {
  name: 'extract-poetry',
  configure: config => {
    config.addNode(ExtractPoetry)

    config.addCommand('extract-poetry', WaxSwitchTextTypeCommand, {
      spec: { type: 'extract-poetry' },
      commandGroup: 'text-types',
    })
    config.addComponent(ExtractPoetry.type, ExtractPoetryComponent)
    config.addConverter('html', ExtractPoetryHTMLConverter)

    config.addLabel('extract-poetry', {
      en: 'Extract: Poetry',
    })
  },
}
