import WaxAnnotationCommand from '../../commands/WaxAnnotationCommand'

import { EditAnnotationCommand } from 'substance'
import Link from './Link'
import LinkComponent from './LinkComponent'
import LinkCommand from './LinkCommand'
import LinkHTMLConverter from './LinkHTMLConverter'
import EditLinkTool from './EditLinkTool'

export default {
  name: 'link',
  configure: function(config) {
    config.addNode(Link)
    config.addComponent('link', LinkComponent)
    config.addConverter('html', LinkHTMLConverter)
    config.addCommand('link', LinkCommand, {
      nodeType: 'link',
      commandGroup: 'annotations',
    })
    config.addCommand('edit-link', EditAnnotationCommand, {
      nodeType: 'link',
      commandGroup: 'prompt',
    })
    config.addTool('edit-link', EditLinkTool)
    config.addIcon('link', { fontawesome: 'fa-link' })
    config.addIcon('open-link', { fontawesome: 'fa-external-link' })
    config.addLabel('link', {
      en: 'Link',
    })
    config.addLabel('open-link', {
      en: 'Open Link',
    })
    config.addLabel('delete-link', {
      en: 'Remove Link',
    })
    config.addKeyboardShortcut('CommandOrControl+K', { command: 'link' })
  },
  Link,
  LinkComponent,
  LinkCommand,
  LinkHTMLConverter,
  EditLinkTool,
}
