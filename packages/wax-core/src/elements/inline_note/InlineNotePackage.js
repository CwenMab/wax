import InlineNote from './InlineNote'
import InlineNoteCommand from './InlineNoteCommand'
import InlineNoteComponent from './InlineNoteComponent'
import InlineNoteHTMLConverter from './InlineNoteHTMLConverter'
import InlineNoteTool from './InlineNoteTool'

export default {
  name: 'inline-note',
  configure: function(config) {
    config.addToolGroup('inline-note')
    config.addNode(InlineNote)

    config.addComponent(InlineNote.type, InlineNoteComponent)
    config.addConverter('html', InlineNoteHTMLConverter)
    config.addCommand(InlineNote.type, InlineNoteCommand, {
      nodeType: InlineNote.type,
    })
    config.addTool('inline-note', InlineNoteTool, { toolGroup: 'inline-note' })
    config.addIcon('inline-note', { fontawesome: 'fa-bookmark' })
    config.addLabel('inline-note', {
      en: 'Inline  note',
    })
  },
}
