import { debounce, isString, forEach, chunk } from 'lodash'

class SpellCheckManager {
  constructor(editorSession, options) {
    options = options || {}
    let wait = options.wait || 750
    this.editorSession = editorSession

    // TODO: MarkersManager is basically a TextPropertyManager
    this.textPropertyManager = editorSession.markersManager
    this.markersManager = editorSession.markersManager

    this._schedule = {}
    this._scheduleCheck = debounce(this._runSpellCheck.bind(this), wait)

    editorSession.onFinalize('document', this._onDocumentChange, this)
  }

  dispose() {
    this.editorSession.off(this)
  }

  check(path) {
    this._runSpellCheck(String(path))
  }

  runGlobalCheck() {
    let paths = Object.keys(this.textPropertyManager._textProperties)
    const editor = this.editorSession._context.editorSession.editor
    editor.emit('displayNotification', () => {
      return `Scanning the document for erros. Please Wait`
    })
    // console.log(chunk(paths, '3')[0])
    setTimeout(() => {
      chunk(paths, '20')[0].forEach((p, index) => {
        this._runSpellCheck(p)
      })
    }, 2000)
  }

  _onDocumentChange(change, info) {
    if (!window.typo) return
    const textProperties = this.textPropertyManager._textProperties
    if (!change.updated) return
    Object.keys(change.updated).forEach(pathStr => {
      if (textProperties[pathStr]) this._scheduleCheck(pathStr)
    })
  }

  _runSpellCheck(pathStr) {
    let path = pathStr.split(',')
    let text = this.editorSession.getDocument().get(path)
    let lang = this.editorSession.getLanguage()
    if (!text || !isString(text)) return
    if (!window.typo) return

    let startlength = 0

    const separators = [
      ' ',
      '-',
      '/',
      ':',
      '\\.',
      '\\,',
      '\\;',
      '\\+',
      '\\—',
      '\\[',
      '\\]',
      '\\”',
      '\\“',
      '\\!',
      '\\‘',
      '\\’',
      '\\(',
      '\\)',
      '\\*',
      '\\?',
    ]
    const textArray = text.split(new RegExp(separators.join('|'), 'g'))
    const data = typo.checkMultiple(textArray, path)
    this._addSpellErrors(path, data)
    return
  }

  /*
    Called when spell corrections have been returned.

    Removes all spell errors on the given path first.
  */
  _addSpellErrors(path, data) {
    const editorSession = this.editorSession
    const markersManager = editorSession.markersManager
    // NOTE: we have one set of markers for each text property
    // as we analyze each text block one by one
    const key = 'spell-error:' + path.join('.')
    const markers = data.map(m => {
      return {
        type: 'spell-error',
        start: {
          path: path,
          offset: m.start,
        },
        end: {
          offset: m.start + m.word.length,
        },
        suggestions: m.suggestions,
      }
    })

    markersManager.setMarkers(key, markers)

    setTimeout(() => {
      editorSession.startFlow()
    })
  }
}

export default SpellCheckManager
