import { forEach } from 'lodash'
import { Command } from 'substance'

class SpellCheckCommand extends Command {
  getCommandState(params) {
    let markFound = undefined
    let state = params.selectionState
    let markers = []
    if (!params.editorSession.markersManager) return
    let Allmarkers =
      params.editorSession.markersManager._markers._documentMarkers

    const selection = params.editorSession.getSelection()

    forEach(Allmarkers, marker => {
      markers.push(...marker)
    })

    if (markers.length === 0) {
      return {
        disabled: true,
      }
    }

    forEach(markers, marker => {
      if (
        selection.start &&
        selection.start.path[0] === marker.start.path[0] &&
        selection.start.offset <= marker.end.offset &&
        selection.start.offset >= marker.start.offset
      ) {
        markFound = marker
        return false
      }
    })
    if (markFound) {
      return {
        disabled: false,
        active: true,
        mode: null,
        node: markFound,
      }
    } else {
      return {
        disabled: true,
      }
    }
  }
}

export default SpellCheckCommand
