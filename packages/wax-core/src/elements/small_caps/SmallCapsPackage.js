import { AnnotationTool } from 'substance'
import SmallCaps from './SmallCaps'
import SmallCapsComponent from './SmallCapsComponent'
import SmallCapsHTMLConverter from './SmallCapsHTMLConverter'
import SmallCapsTool from './SmallCapsTool'
import SmallCapsCommand from './SmallCapsCommand'
import WaxAnnotationCommand from '../../commands/WaxAnnotationCommand'

export default {
  name: 'small-caps',
  configure: (config, { disableCollapsedCursor, toolGroup }) => {
    config.addNode(SmallCaps)

    config.addComponent(SmallCaps.type, SmallCapsComponent)

    config.addCommand('small-caps', WaxAnnotationCommand, {
      nodeType: 'small-caps',
      disableCollapsedCursor,
      commandGroup: 'annotations',
    })

    config.addConverter('html', SmallCapsHTMLConverter)

    config.addTool('small-caps', SmallCapsTool)

    config.addIcon('small-caps', { fontawesome: 'fa-text-height' })
  },
}
