import {
  BlockNodeComponent,
  deleteNode,
  FontAwesomeIcon as Icon,
} from 'substance'

class ImageComponent extends BlockNodeComponent {
  constructor(...props) {
    super(...props)
    this.imageId = undefined
    this.flag = false
  }
  didMount() {
    super.didMount.call(this)
    this.context.editorSession.onRender(
      'document',
      this._onDocumentChange,
      this,
    )
    this.context.editorSession.onUpdate('', this.displayCaptionArea, this)
  }

  dispose() {
    super.dispose.call(this)
    this.context.editorSession.off(this)
  }

  _onDocumentChange(change) {
    if (
      change.hasUpdated(this.props.node.id) ||
      change.hasUpdated(this.props.node.imageFile)
    ) {
      this.rerender()
    }
  }

  render($$) {
    let el = $$('figure')
    let caption = $$('span')
    const captionClass = this.props.node.caption === ' ' ? 'figcaption' : ''

    if (this.props.node.caption.length > 0) {
      caption = $$('figcaption')
        .addClass(captionClass)
        .append(this.props.node.caption)
        .append(
          $$(Icon, { icon: 'fa-edit' })
            .addClass('edit-caption')
            .attr('title', 'edit caption')
            .on('click', this.editCaption),
        )
    }
    el.addClass('sc-image')
    el.append(
      $$('img')
        .attr({
          src: this.props.node.getUrl(),
        })
        .ref('image'),
    ).append(caption)

    const editor = this.getEditor()
    editor.emit('ui:updated')

    if (this.flag && this.imageId === this.props.node.id) {
      caption.css('display', 'none')
    }
    return el
  }

  displayCaptionArea() {
    const selection = this.context.editorSession.getSelection()
    const doc = this.context.editorSession.getDocument()

    if (typeof this.imageId !== 'undefined') {
      this.rerender()
      this.imageId = undefined
    }
    if (selection.type !== 'node') return

    const node = doc.get(selection.nodeId)
    if (node && node.type === 'image') {
      this.imageId = node.id
      this.flag = true
      this.rerender()
    }
    return
  }

  editCaption() {
    const containerId = this.props.node.parent.id
    this.context.editorSession.transaction(tx => {
      tx.setSelection({
        type: 'node',
        containerId: containerId,
        nodeId: this.props.node.id,
      })
    })
  }

  getEditor() {
    return this.context.editor
  }
}

export default ImageComponent
