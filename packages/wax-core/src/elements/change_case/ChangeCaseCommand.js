import { Command } from 'substance'

class ChangeCaseCommand extends Command {
  getCommandState(params) {
    let newState = {
      active: false,
      disabled: false,
    }

    const selection = params.editorSession.getSelection()

    if (selection.isCollapsed() || selection.isContainerSelection()) {
      newState.disabled = true
    }

    if (params.surface) {
      const editorProps = params.surface.context.editor.props
      if (editorProps.editing === 'selection') newState.disabled = true
    }

    return newState
  }

  execute(params) {
    return true
  }
}

export default ChangeCaseCommand
