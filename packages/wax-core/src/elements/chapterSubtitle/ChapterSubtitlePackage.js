import ChapterSubtitle from './ChapterSubtitle'
import ChapterSubtitleComponent from './ChapterSubtitleComponent'
import ChapterSubtitleHTMLConverter from './ChapterSubtitleHTMLConverter'
import WaxSwitchTextTypeCommand from '../../commands/WaxSwitchTextTypeCommand'

export default {
  name: 'chapter-subtitle',
  configure: config => {
    config.addNode(ChapterSubtitle)
    config.addCommand('chapter-subtitle', WaxSwitchTextTypeCommand, {
      spec: { type: 'chapter-subtitle' },
      commandGroup: 'text-display',
    })
    config.addComponent(ChapterSubtitle.type, ChapterSubtitleComponent)
    config.addConverter('html', ChapterSubtitleHTMLConverter)

    config.addLabel('chapter-subtitle', {
      en: 'Subtitle',
    })
  },
}
