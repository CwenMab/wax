import ExtractProse from './ExtractProse'
import ExtractProseComponent from './ExtractProseComponent'
import ExtractProseHTMLConverter from './ExtractProseHTMLConverter'
import WaxSwitchTextTypeCommand from '../../commands/WaxSwitchTextTypeCommand'

export default {
  name: 'extract-prose',
  configure: config => {
    config.addNode(ExtractProse)
    config.addCommand('extract-prose', WaxSwitchTextTypeCommand, {
      spec: { type: 'extract-prose' },
      commandGroup: 'text-types',
    })
    config.addComponent(ExtractProse.type, ExtractProseComponent)
    config.addConverter('html', ExtractProseHTMLConverter)

    config.addLabel('extract-prose', {
      en: 'Extract: Prose',
    })
  },
}
