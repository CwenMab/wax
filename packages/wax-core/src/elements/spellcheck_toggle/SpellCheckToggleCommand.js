import { Command } from 'substance'
import { each } from 'lodash'

class SpellCheckToggleCommand extends Command {
  getCommandState(params, config) {
    const newState = {
      disabled: false,
      active: false,
    }

    if (params.surface)
      if (params.surface && params.surface.props.spellcheck) {
        newState.active = true
      }
    return newState
  }

  execute(params, context) {
    const selection = params.editorSession.getSelection()
    const surface = context.surfaceManager.getSurface('main')
    const spellCheckStatus = surface.props.spellcheck
    params.surface.context.editor.extendState({ spellCheck: !spellCheckStatus })

    params.editorSession.setSelection(selection)
    return true
  }
}

SpellCheckToggleCommand.type = 'spell-check-toggle'

export default SpellCheckToggleCommand
