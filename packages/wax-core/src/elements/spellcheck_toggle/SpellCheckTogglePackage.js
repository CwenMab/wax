import { platform, Tool } from 'substance'
import SpellCheckToggleCommand from './SpellCheckToggleCommand'
import SpellCheckToggleTool from './SpellCheckToggleTool'

export default {
  name: 'spell-check-toggle',
  configure: config => {
    config.addCommand('spell-check-toggle', SpellCheckToggleCommand, {
      commandGroup: 'spell-check-toggle',
    })
    config.addTool('spell-check-toggle', SpellCheckToggleTool)
    config.addIcon('spell-check-toggle', { fontawesome: 'fa-check' })
    config.addLabel('spell-check-toggle', {
      en: 'Toggle spell check',
    })

    let controllerKey = 'ctrl'
    if (platform.isMac) controllerKey = 'cmd'
    //TODO what is the shrotcut?
  },
}
