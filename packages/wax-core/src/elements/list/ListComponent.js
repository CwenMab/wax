/* eslint react/prop-types: 0 */
import { isString, NodeComponent } from 'substance'
import ListItemComponent from './ListItemComponent'
import renderListNode from './renderListNode'

export default class ListComponent extends NodeComponent {
  render($$) {
    let node = this.props.node
    let el = renderListNode(node, item => {
      // item is either a list item node, or a tagName
      if (isString(item)) {
        return $$(item)
      } else if (item.type === 'list-item') {
        return $$(ListItemComponent, {
          node: item,
        }).ref(item.id)
      }
    })
    el.addClass('sc-list').attr('data-id', node.id)
    return el
  }
}

// we need this ATM to prevent this being wrapped into an isolated node (see ContainerEditor._renderNode())
ListComponent.prototype._isCustomNodeComponent = true
