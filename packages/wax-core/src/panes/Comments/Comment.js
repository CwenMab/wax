/* eslint react/prop-types: 0 */

import { Component } from 'substance'
// import { trim } from 'lodash'

class Comment extends Component {
  constructor(...args) {
    super(...args)

    this.commentHeight = 0
    this.computedLineHeight = 0
  }

  adjustTops() {
    const { setTops } = this.props
    setTops()
  }

  onInput(e) {
    const commentEditTextarea = e.target

    let enabled = true

    if (commentEditTextarea.value.trim().length === 0) {
      enabled = false
    }

    this.adjustTextareaHeight()
    this.adjustTops()
    this.changeSaveBtnState(enabled)
  }

  onKeyDown(e) {
    if (e.keyCode === 13) {
      // enter
      const textAreaValue = e.target.value
      if (textAreaValue.trim().length > 0) {
        this.saveChange(e)
      }
    }
  }

  getTextareaEl() {
    return this.find('.comment-edit-textarea').el.el
  }

  isOpen() {
    const { editableComment, index } = this.props
    return editableComment === index
  }

  didMount() {
    if (!this.isOpen()) return

    this.adjustTextareaHeight()
    this.focus()
  }

  // TODO -- save btn should be a component. state could be handled as a prop
  changeSaveBtnState(enabled) {
    const saveBtn = this.find('.comment-save').el

    if (enabled) {
      saveBtn.removeAttr('disabled')
    } else {
      saveBtn.attr('disabled', true)
    }
  }

  discardChange(e) {
    e.preventDefault()
    e.stopPropagation()

    const { updateEditableComment } = this.props
    updateEditableComment(null)
  }

  editComment(e) {
    e.preventDefault()
    e.stopPropagation()

    const { index, updateEditableComment } = this.props
    updateEditableComment(index)
  }

  saveChange(e) {
    e.preventDefault()
    e.stopPropagation()

    const { editComment, index, updateEditableComment } = this.props
    const value = this.refs.commentEdit.getValue()

    editComment(value, index)

    updateEditableComment(null)
  }

  adjustTextareaHeight() {
    const commentEditTextarea = this.getTextareaEl()

    commentEditTextarea.style.height = '1px'
    commentEditTextarea.style.height = `${commentEditTextarea.scrollHeight}px`

    this.adjustTops()
  }

  focus() {
    const commentEditTextarea = this.getTextareaEl()
    commentEditTextarea.focus()
  }

  render($$) {
    const { active, editableComment, index, user } = this.props
    const editable = editableComment === index

    const authorId = user.id
    const name = user.username

    // could do more elegant? with props?
    const currentUserId = this.context.editor.props.user.id

    let interactionsArea
    let text = this.props.text // make const

    if (!active) {
      // preview
      if (text.length > 100) {
        text = `${text.substring(0, 100)}...`
      }
    }

    if (active && currentUserId === authorId) {
      interactionsArea = $$('div').addClass('sc-comment-interactions-area')

      if (editable) {
        const discardBtn = $$('button')
          .addClass('comment-discard')
          .on('click', this.discardChange)

        const saveBtn = $$('button')
          .addClass('comment-save')
          .on('click', this.saveChange)

        interactionsArea.append(discardBtn, saveBtn)
      } else {
        const editBtn = $$('button')
          .addClass('comment-edit')
          .attr('title', 'edit')
          .on('mousedown', this.editComment) // on click?

        interactionsArea.append(editBtn)
      }
    }

    const nameEl = $$('div')
      .addClass('comment-user-name')
      .append(name)

    const commentTextWrapper = $$('div').addClass('comment-text-wrapper')

    let textEl

    if (editable) {
      textEl = $$('textarea')
        .addClass('comment-edit-textarea')
        .attr('rows', '1')
        .on('keydown', this.onKeyDown)
        .on('input', this.onInput)
        .on('focus', this.adjustTextareaHeight)
        .append(text)
        .ref('commentEdit')
    } else {
      textEl = $$('div')
        .addClass('comment-text')
        .append(text)
    }

    commentTextWrapper.append(textEl, interactionsArea)

    // TODO -- do we simply wrap the entry in another div to give a class?
    const entry = $$('div')
      .addClass('sc-comment-entry')
      .append(nameEl, commentTextWrapper)

    return $$('div')
      .addClass('single-comment-row')
      .append(entry)
  }
}

export default Comment
