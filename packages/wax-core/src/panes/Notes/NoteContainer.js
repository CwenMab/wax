import { Container } from 'substance'

class NoteContainer extends Container {}
NoteContainer.define({
  type: 'note-container',
  nodes: { type: ['array', 'id'], default: [], owned: true },
  groupWithSameType: {
    type: 'boolean',
    default: true,
  },
  groupName: {
    type: 'string',
    default: 'notes',
  },
  order: {
    type: 'number',
    default: 0,
  },
  disableStyling: {
    type: 'boolean',
    default: true,
  },
})

export default NoteContainer
